namespace LexiconZoo
{
    public static class Constants
    {
        public enum AdapterFilter
        {
            ALL_ANIMALS = 1,
            TAXONOMY = 2,
            LOCALITY = 3
        }

        public const string ImagesApiUrl = "https://agile-chamber-32011.herokuapp.com/api/animals/images/";
    }
}