using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LexiconZoo.Classes;

namespace LexiconZoo.Adapters
{
    /// <summary>
    /// Adapter for TaxonomyListActivity
    /// </summary>
    class TaxonomyAdapter : BaseAdapter
    {
        Context context;
        IEnumerable<string> classes;

        public TaxonomyAdapter(Context context)
        {
            this.context = context;
            // select all animal taxonomy classes
            this.classes = DBHandlerGetter.GetHandler().GetAllAnimals().ToList().Select(x=>x.Class).OrderBy(x=>x).Distinct();
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return classes.ElementAt(position);
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;
            TaxonomyAdapterViewHolder holder = null;

            if (view != null)
                holder = view.Tag as TaxonomyAdapterViewHolder;

            if (holder == null)
            {
                holder = new TaxonomyAdapterViewHolder();
                var inflater = context.GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();

                // inflate one of predefined view
                view = inflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);

                holder.Title = view.FindViewById<TextView>(Android.Resource.Id.Text1);
                view.Tag = holder;              
            }
            holder.Title.Text = classes.ElementAt(position);// "new text here";
            return view;
        }

        public override int Count
        {
            get
            {
                return classes.Count();
            }
        }
    }

    class TaxonomyAdapterViewHolder : Java.Lang.Object
    {
        //adapter views to re-use
        public TextView Title { get; set; }
    }
}