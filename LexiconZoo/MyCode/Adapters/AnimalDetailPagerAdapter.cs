using Android.Content;
using Android.Support.V4.App;

namespace LexiconZoo.Adapters
{
    class AnimalDetailPagerAdapter : FragmentStatePagerAdapter
    {
        public int animalId;
        Context context;
        private Fragment abFragment;
        private Fragment adFragment;
        private Fragment azFragment;
        public AnimalDetailPagerAdapter(FragmentManager fm, int animalId, Context context)
            : base(fm)
        {
            this.animalId = animalId == 0 ? 1 : animalId;
            this.context = context;
        }

        public override int Count
        {
            get { return 3; }
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
        {
            switch (position)
            {
                case 0: return new Java.Lang.String(context.Resources.GetString(Resource.String.basicInfo));
                case 1: return new Java.Lang.String(context.Resources.GetString(Resource.String.descriptionInfo));
                case 2: return new Java.Lang.String(context.Resources.GetString(Resource.String.zooInfo));
                default: return new Java.Lang.String(context.Resources.GetString(Resource.String.basicInfo));
            }            
        }

        /// <summary>
        /// Select and return fragment for current position. Create new if it has not been already created, else use the old one.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            switch (position)
            {
                case 0:
                    return abFragment ?? (abFragment = (Android.Support.V4.App.Fragment)
                                            Fragments.AnimalBasicInfoFragment.NewInstance(
                                            animalId));
                case 1:
                    return adFragment ?? (adFragment = (Android.Support.V4.App.Fragment)
                                            Fragments.AnimalDescFragment.newInstance(
                                            animalId));                   
                case 2:
                    return azFragment ?? (azFragment = (Android.Support.V4.App.Fragment)
                                            Fragments.AnimalZooFragment.newInstance(
                                            animalId));                    
                default:
                    return abFragment ?? (abFragment = (Android.Support.V4.App.Fragment)
                                            Fragments.AnimalBasicInfoFragment.NewInstance(
                                            animalId));
            }       
        }
    }    
}