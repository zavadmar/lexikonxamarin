using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using LexiconZoo.Activities;
using Square.Picasso;
using LexiconZoo.Classes;
using System.Globalization;

namespace LexiconZoo.Adapters
{
    class AnimalListViewAdapter : BaseAdapter, ISectionIndexer, IFilterable
    {
        Context context;        
        private Dictionary<string, int> alphaIndex;
        private string[] sections;
        private Java.Lang.Object[] sectionsObjects;
        List<AnimalListBasics> animalsBasics;

        private ViewType viewType;
        public enum ViewType
        {
            LISTVIEW = 1,
            CARDVIEW = 2
        }

        public AnimalListViewAdapter(Context context, ViewType vType = ViewType.CARDVIEW)
        {
            this.context = context;
            var activity = context as AnimalListActivity;
            animalsBasics = new List<AnimalListBasics>();
            viewType = vType;
            IQueryable<Animal> animals;           

            animals = GetFilteredAnimals();

            foreach (var animal in animals)
            {
                animalsBasics.Add(new AnimalListBasics(animal._id, animal.Title, animal.ImageUrl));
            }

            createIndex();            
        }

        /// <summary>
        /// Return animal IQueryable collection, get filter settings from AnimalListActivity and use it to filter.
        /// </summary>
        /// <returns>IQueryable collection of animals.</returns>
        public IQueryable<Animal> GetFilteredAnimals()
        {          
            var activity = context as AnimalListActivity;
            string filterValue = activity.GetFilterValue();            
            var dbHandler = DBHandlerGetter.GetHandler();
            if (dbHandler.GetAnimalCnt() > 0)
            {
                switch (activity.GetFilter())
                {
                    case Constants.AdapterFilter.ALL_ANIMALS:
                        return dbHandler.GetAllAnimals();
                    case Constants.AdapterFilter.LOCALITY:
                        activity.Title = filterValue;
                        return dbHandler.GetAllAnimals().Where(x => x.LocalitiesTitle.Contains(filterValue));
                    case Constants.AdapterFilter.TAXONOMY:
                        activity.Title = filterValue;
                        return dbHandler.GetAllAnimals().Where(x => x.Class == filterValue);
                }
            }
            return null;
        }


        /// <summary>
        /// Create index for fast scroll.
        /// </summary>
        protected void createIndex()
        {
            alphaIndex = new Dictionary<string, int>();
            for (int i = 0; i < animalsBasics.Count(); i++)
            { 
                var key = animalsBasics.ElementAt(i).Title[0].ToString();
                if (!alphaIndex.ContainsKey(key))
                    alphaIndex.Add(key, i); // add each 'new' letter to the index
            }
            // convert letters list to string[]
            sections = new string[alphaIndex.Keys.Count];            
            alphaIndex.Keys.CopyTo(sections, 0); 

            // Interface requires a Java.Lang.Object[], so we create one here
            sectionsObjects = new Java.Lang.Object[sections.Length];
            for (int i = 0; i < sections.Length; i++)
            {
                sectionsObjects[i] = new Java.Lang.String(sections[i]);
            }
        }                   

        public override Java.Lang.Object GetItem(int position)
        {           
            return animalsBasics.ElementAt(position).Title;
        }

        public override long GetItemId(int position)
        {
            return animalsBasics.ElementAt(position).Id;
        }
       
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;
            AnimalListViewAdapterViewHolder holder = null;

            if (view != null)
                holder = view.Tag as AnimalListViewAdapterViewHolder;

            if (holder == null)
            {
                holder = new AnimalListViewAdapterViewHolder();
                var inflater = context.GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();  
                // use selected view type
                switch (this.viewType)
                {
                    case ViewType.CARDVIEW:
                        view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.AnimalListCard, parent, false);
                        break;
                    case ViewType.LISTVIEW:
                        view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.AnimalListRow, parent, false);
                        break;                    
                }

                holder.Title = view.FindViewById<TextView>(Resource.Id.title);
                holder.Img = view.FindViewById<ImageView>(Resource.Id.imageView);
              
                view.Tag = holder;
            }

            //fill in your items
            holder.Title.Text = animalsBasics.ElementAt(position).Title;

            // try to load image if image url exists
            if (animalsBasics.ElementAt(position).ImgUrl != null && animalsBasics.ElementAt(position).ImgUrl != "")
            {
                try
                {
                    holder.Img.Visibility = ViewStates.Visible;
                    var pic = Picasso.With(context);
#if DEBUG
                    pic.IndicatorsEnabled = true;
#endif
                    pic.Load(Constants.ImagesApiUrl + animalsBasics.ElementAt(position).Id)
                        .Into(holder.Img);
                }
                catch
                {
                    Picasso.With(context)
                      .CancelRequest(holder.Img);
                    holder.Img.SetImageDrawable(null);
                }
            }
            else
            {
                Picasso.With(context).CancelRequest(holder.Img);
                holder.Img.SetImageDrawable(null);
            }

            return view;
        }

        /// <summary>
        /// Used by fast scroller.
        /// </summary>
        /// <param name="sectionIndex"></param>
        /// <returns>Position in list for section index.</returns>
        public int GetPositionForSection(int sectionIndex)
        {
            return alphaIndex[sections[sectionIndex]];
        }

        /// <summary>
        /// Used by fast scroller.
        /// </summary>
        /// <param name="position"></param>
        /// <returns>Return section for current position.</returns>
        public int GetSectionForPosition(int position)
        {
            int prevSection = 0;
            for (int i = 0; i < sections.Length; i++)
            {
                if (GetPositionForSection(i) > position)
                {
                    break;
                }
                prevSection = i;
            }
            return prevSection;
        }

        /// <summary>
        /// Used by fast scroller.
        /// </summary>
        /// <returns>Return section array.</returns>
        public Java.Lang.Object[] GetSections()
        {
            return sectionsObjects;
        }
        
        public override int Count
        {
            get
            {
                return animalsBasics.Count();
            }
        }

        public Filter Filter
        {
            get
            {
                return new MyFilter(this);
            }
        }

        /// <summary>
        /// Create new index after filtering and call base class implementation.
        /// </summary>
        public override void NotifyDataSetChanged()
        {
            createIndex();
            base.NotifyDataSetChanged();
        }

        /// <summary>
        /// Filter implementation.
        /// </summary>
        class MyFilter : Filter
        {
            AnimalListViewAdapter adapter; // reference to creating adapter
            public MyFilter(AnimalListViewAdapter a)
            {
                this.adapter = a;
                
            }

            protected override FilterResults PerformFiltering(ICharSequence constraint)
            {
                var returnObj = new FilterResults();
                var results = new List<AnimalListBasics>();

                // query is empty
                if (constraint == null) return returnObj;

                // get animals
                var animals = adapter.GetFilteredAnimals();                

                if (animals != null && animals.Count() >= 0)
                {
                    // filter by constraint
                    var al = animals.ToList().Where(x=> RemoveDiacritics(x.Title.ToLower()).Contains(RemoveDiacritics(constraint.ToString().ToLower())));
                    
                    foreach (var animal in al)
                    {
                        results.Add(new AnimalListBasics(animal._id, animal.Title, animal.ImageUrl));
                    }                    
                }

                // convert to Java.Object
                returnObj.Values = FromArray(results.Select(r => r.ToJavaObject()).ToArray());
                returnObj.Count = results.Count;

                // dispose to save memory using
                constraint.Dispose();
                return returnObj;
            }

            static string RemoveDiacritics(string text)
            {
                return string.Concat(
                    text.Normalize(NormalizationForm.FormD)
                    .Where(ch => CharUnicodeInfo.GetUnicodeCategory(ch) !=
                                                  UnicodeCategory.NonSpacingMark)
                  ).Normalize(NormalizationForm.FormC);
            }

            protected override void PublishResults(ICharSequence constraint, FilterResults results)
            {
                using (var values = results.Values)
                {              
                    if (values != null)
                    {
                        // convert filtering results back to .NET objects
                        adapter.animalsBasics = values.ToArray<Java.Lang.Object>()
                            .Select(r => r.ToNetObject<AnimalListBasics>()).ToList();
                    }
                    else
                    {
                        if (adapter.animalsBasics != null)
                        {
                            if (adapter.animalsBasics.Count > 0) adapter.animalsBasics.RemoveRange(0, adapter.animalsBasics.Count);
                        }
                        // no animals
                        else
                        {
                            adapter.animalsBasics =  new List<AnimalListBasics>();
                        }
                    }                    
                }

                // call to change data in listview
                adapter.NotifyDataSetChanged();

                // dispose to save memory using
                constraint.Dispose();
                results.Dispose();
            }
        }
    }

    class AnimalListViewAdapterViewHolder : Java.Lang.Object
    {
        //adapter views to re-use
        public TextView Title { get; set; }
        public ImageView Img { get; set; }
    }

    // class to cache animals for listview
    class AnimalListBasics
    {
        public int Id;
        public string Title;
        public string ImgUrl;
        public AnimalListBasics(int id, string title, string url)
        {
            this.Id = id;
            this.Title = title;
            this.ImgUrl = url;
        }
    }
}