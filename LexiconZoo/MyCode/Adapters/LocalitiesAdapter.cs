using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LexiconZoo.Classes;

namespace LexiconZoo.Adapters
{
    /// <summary>
    /// Adapter for LocalityListActivity 
    /// </summary>
    class LocalitiesAdapter : BaseAdapter
    {
        Context context;
        IEnumerable<string> localities;

        public LocalitiesAdapter(Context context)
        {
            this.context = context;
            // select localities
            this.localities = DBHandlerGetter.GetHandler().GetAllAnimals().Where(x => x.LocalitiesTitle != "" && x.LocalitiesTitle != null).ToList().Select(x=>x.LocalitiesTitle).OrderBy(x => x).Distinct();
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return localities.ElementAt(position);
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;
            LocalitiesAdapterViewHolder holder = null;

            if (view != null)
                holder = view.Tag as LocalitiesAdapterViewHolder;

            if (holder == null)
            {
                holder = new LocalitiesAdapterViewHolder();
                var inflater = context.GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();

                // inflate one of predefined view
                view = inflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);
                                
                holder.Title = view.FindViewById<TextView>(Android.Resource.Id.Text1);
                view.Tag = holder;              
            }
            //fill in items
            holder.Title.Text = localities.ElementAt(position);
            return view;
        }

        public override int Count
        {
            get
            {
                return localities.Count();
            }
        }
    }

    class LocalitiesAdapterViewHolder : Java.Lang.Object
    {
        public TextView Title { get; set; }
    }
}