using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LexiconZoo.Classes;

namespace LexiconZoo.Adapters
{
    /// <summary>
    /// Adapter for RssListActivity
    /// </summary>
    class RssListViewAdapter : BaseAdapter
    {
        Context context;
        List<Rss> rssList;
        private IDBHandler dbHandler;
        
        public RssListViewAdapter(Context context)
        {
            this.context = context;           
            dbHandler = DBHandlerGetter.GetHandler();

            if (dbHandler.GetRssCnt() > 0)
            {
                this.rssList = dbHandler.GetAllRss().ToList();
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return rssList.ElementAt(position).title;
        }

        public override long GetItemId(int position)
        {          
            return position;
        }
       
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;
            RssListViewAdapterViewHolder holder = null;

            if (view != null)
                holder = view.Tag as RssListViewAdapterViewHolder;

            if (holder == null)
            {
                holder = new RssListViewAdapterViewHolder();
                var inflater = context.GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();

                // inflate one of predefined view
                view = inflater.Inflate(Android.Resource.Layout.SimpleListItem2, null);

                holder.Title = view.FindViewById<TextView>(Android.Resource.Id.Text1);
                holder.Date = view.FindViewById<TextView>(Android.Resource.Id.Text2);
                view.Tag = holder;
            }
            //fill in your items
            holder.Title.Text = rssList.ElementAt(position).title;
            holder.Date.Text = DateTime.Parse(rssList.ElementAt(position).date).ToShortDateString();
            return view;
        }                 
        public override int Count
        {
            get
            {
                return rssList.Count();
            }
        }
    }

    class RssListViewAdapterViewHolder : Java.Lang.Object
    {
        //adapter views to re-use
        public TextView Title { get; set; }
        public TextView Date { get; set; }
    }
}