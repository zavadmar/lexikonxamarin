using System;
using Android.Content;
using Android.Support.V4.App;

namespace LexiconZoo.Adapters
{
    /// <summary>
    /// Adapter for creating calendar month fragments.
    /// </summary>
    class CalendarAdapter : FragmentPagerAdapter
    {
        private const int count = 24;
        private int currentMonth;
        private int currentYear;
        private Fragment[] fragments;
        private Context context;

        public CalendarAdapter(FragmentManager fm, Context context)
            : base(fm)
        {
            this.currentMonth = DateTime.Now.Month;
            this.currentYear = DateTime.Now.Year;
            this.context = context;         
            fragments = new Fragment[count];
            for (int i = 0; i< count; i++)
            {
                fragments[i] = null;
            }
        }

        public override int Count
        {
            get { return count; }
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
        {
            var m = ( currentMonth + position );
            var y = currentYear - 1;
            while (m > 12)
            {
                m = m - 12;
                y++;
            }            
            return new Java.Lang.String(new DateTime(y, m, 1).ToString("MMMM yyyy"));                        
        }

        /// <summary>
        /// Select and return CalendarFragment for month counted by position. Create new fragment if it has not been alredy created.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public override Fragment GetItem(int position)
        {            
            if (fragments[position] == null)
            {
                var m = currentMonth + position;
                var y = currentYear - 1;
                while (m > 12)
                {
                    m = m - 12;
                    y++;
                }
                fragments[position] = Fragments.CalendarFragment.NewInstance(m, y);
            }
            return fragments[position];
        }
    }    
}