using System;
using Android.OS;
using Android.Views;
using Android.Widget;
using LexiconZoo.Classes;
using Android.Text.Util;
using Android.Support.V4.App;
using System.Linq;

namespace LexiconZoo.Fragments
{
    public class RssDetailFragment : Fragment
    {
        private static String RSS_ID = "rss_id";
        private IDBHandler dbHandler;

        public RssDetailFragment() {
            dbHandler = DBHandlerGetter.GetHandler();
        }
        public static RssDetailFragment NewInstance(String rssId)
        {
            if (rssId == null  || rssId == "")
            {
                rssId = DBHandlerGetter.GetHandler().GetAllRss().First()._id;
            }
            RssDetailFragment fragment = new RssDetailFragment();            
            Bundle args = new Bundle();
            args.PutString(RSS_ID, rssId);            
            fragment.Arguments = args;
            return fragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (container == null)
            {
                // Currently in a layout without a container, so no reason to create our view.
                return null;
            }
            var scroller = new ScrollView(Activity);
            var rss = dbHandler.GetRssById(getRssId);
            if (rss == null)
            {
                rss = dbHandler.GetAllRss().FirstOrDefault();
                if (rss == null)
                {
                    Toast.MakeText(Context, Resource.String.NoItemDownloaded, ToastLength.Long);
                    return scroller;
                }
                Toast.MakeText(Context, Resource.String.ShowingFirst, ToastLength.Long);               
            }

            var view = inflater.Inflate(Resource.Layout.RssDetail, container, false);
            var titleView = view.FindViewById<TextView>(Resource.Id.title);
            var dateView = view.FindViewById<TextView>(Resource.Id.date);
            var descView = view.FindViewById<TextView>(Resource.Id.description);
            var linkView = view.FindViewById<TextView>(Resource.Id.link);
            titleView.Text = rss.title;
            dateView.Text = DateTime.Parse(rss.date).ToShortDateString();
            descView.Text = rss.description;
            linkView.Text = rss.link;
            Linkify.AddLinks(linkView, MatchOptions.WebUrls);
            scroller.AddView(view);

            return scroller;
        }

        public string getRssId
        {
            get { return Arguments.GetString(RSS_ID, ""); }
        }
    }
}