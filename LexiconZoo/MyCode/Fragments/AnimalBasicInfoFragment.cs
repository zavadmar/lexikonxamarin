using System;
using Android.OS;
using Android.Views;
using Android.Widget;
using LexiconZoo.Classes;
using Square.Picasso;

namespace LexiconZoo.Fragments
{
    public class AnimalBasicInfoFragment : Android.Support.V4.App.Fragment
    {
        private static string ANIMAL_ID = "animal_id";
        
        public AnimalBasicInfoFragment() { }
        public static AnimalBasicInfoFragment NewInstance(int animalId)
        {
            AnimalBasicInfoFragment fragment = new AnimalBasicInfoFragment();
            Bundle args = new Bundle();
            args.PutInt(ANIMAL_ID, animalId);            
            fragment.Arguments = args;
            return fragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (container == null)
            {
                // Currently in a layout without a container, so no reason to create our view.
                return null;
            }
            var scroller = new ScrollView(Activity);
            var view = inflater.Inflate(Resource.Layout.AnimalDetail, container, false);
            var titleView = view.FindViewById<TextView>(Resource.Id.Title);
            var latinTitleView = view.FindViewById<TextView>(Resource.Id.LatinTitle);
            var classView = view.FindViewById<TextView>(Resource.Id.animalClass);
            var orderView = view.FindViewById<TextView>(Resource.Id.order);
            var spreadView = view.FindViewById<TextView>(Resource.Id.spread);
            var biotopView = view.FindViewById<TextView>(Resource.Id.biotop);
            var foodView = view.FindViewById<TextView>(Resource.Id.food);
            var proportionsView = view.FindViewById<TextView>(Resource.Id.proportions);
            var imageView = view.FindViewById<ImageView>(Resource.Id.imageView);

            var animal = DBHandlerGetter.GetHandler().GetAnimalById(GetAnimalId);
            try
            {
                var pic = Picasso.With(Activity);
#if DEBUG
                pic.IndicatorsEnabled = true;
#endif
                pic.Load("https://agile-chamber-32011.herokuapp.com/api/animals/images/" + animal._id)                  
                    .Into(imageView);
            }
            catch 
            {
            }
            titleView.Text = animal.Title;
            latinTitleView.Text = animal.LatinTitle;
            classView.Text = Context.GetString(Resource.String.Class) + ": " + animal.Class;
            orderView.Text = Context.GetString(Resource.String.Order) + ": " + animal.Order;
            spreadView.Text = Context.GetString(Resource.String.Spread) + ": " + animal.SpreadNote;
            biotopView.Text = Context.GetString(Resource.String.Biotop) + ": " + animal.BiotopesNotes;
            foodView.Text = Context.GetString(Resource.String.Food) + ": " + animal.FoodNotes;
            proportionsView.Text = Context.GetString(Resource.String.Proportions) + ": " + animal.Proportions;
            scroller.AddView(view);

            return scroller;
        }

        public int GetAnimalId
        {
            get { return Arguments.GetInt(ANIMAL_ID, 1); }
        }
    }
}