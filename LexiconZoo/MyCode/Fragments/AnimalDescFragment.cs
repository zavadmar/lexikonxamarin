using Android.OS;
using Android.Views;
using Android.Widget;
using LexiconZoo.Classes;
using Android.Support.V7.Widget;

namespace LexiconZoo.Fragments
{
    public class AnimalDescFragment : Android.Support.V4.App.Fragment
    {
        private static string ANIMAL_ID = "animal_id";
        
        public AnimalDescFragment() { }
        public static AnimalDescFragment newInstance(int animalId)
        {
            AnimalDescFragment fragment = new AnimalDescFragment();
            Bundle args = new Bundle();
            args.PutInt(ANIMAL_ID, animalId);            
            fragment.Arguments = args;
            return fragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (container == null)
            {
                // Currently in a layout without a container, so no reason to create our view.
                return null;
            }
            var scroller = new ScrollView(Activity);
            var layout = (LinearLayout)inflater.Inflate(Resource.Layout.AnimalTextDetail, container, false);
            var descriptionView = (TextView)layout.FindViewById<TextView>(Resource.Id.description);
            var attractionView = (TextView)layout.FindViewById<TextView>(Resource.Id.attraction);
            var reproductionView = (TextView)layout.FindViewById<TextView>(Resource.Id.reproduction);
            var projectView = (TextView)layout.FindViewById<TextView>(Resource.Id.project);

            var animal = DBHandlerGetter.GetHandler().GetAnimalById(getAnimalId);

            descriptionView.Text = animal.Description;
            attractionView.Text = animal.Attractions;
            reproductionView.Text = animal.Reproduction;
            if (animal.ProjectsNote == null || animal.ProjectsNote == "")
            {
                var projectCard = (CardView)layout.FindViewById<CardView>(Resource.Id.projectCard);
                projectCard.Visibility = ViewStates.Invisible;
            }
            else projectView.Text = animal.ProjectsNote;
           
            scroller.AddView(layout);
            return scroller;           
        }

        public int getAnimalId
        {
            get { return Arguments.GetInt(ANIMAL_ID, 1); }
        }
    }
}