using Android.OS;
using Android.Views;
using Android.Support.V4.View;
using Android.Support.V4.App;

namespace LexiconZoo.Fragments
{
    /// <summary>
    /// Fragment, that contains fragment with months in tabs
    /// </summary>
    public class CalendarTabFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.AnimalViewPager, container, false);
            var viewPager = (ViewPager)view.FindViewById(Resource.Id.viewpager);
            var adapter = new Adapters.CalendarAdapter(FragmentManager, Activity);
            viewPager.Adapter = adapter;
            viewPager.SetCurrentItem(12, false);
            return view;
        }

        internal static CalendarTabFragment NewInstance()
        {
            var detailFrag = new CalendarTabFragment { Arguments = new Bundle() };            
            return detailFrag;
        }              
    }
}