using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Views;

namespace LexiconZoo.Fragments
{
    /// <summary>
    /// Fragmens that store animal detail tabs
    /// </summary>
    public class AnimalDetailTabsFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.AnimalViewPager, container, false);
            var viewPager = (ViewPager)view.FindViewById(Resource.Id.viewpager);
            var adapter = new Adapters.AnimalDetailPagerAdapter(FragmentManager, GetAnimalId, Activity);
            viewPager.Adapter = adapter;

            if (GetAnimalId != 0) Activity.Title = Classes.DBHandlerGetter.GetHandler().GetAnimalById(GetAnimalId).Title;
            return view;
        }
        
        internal static AnimalDetailTabsFragment NewInstance(int id)
        {
            var detailFrag = new AnimalDetailTabsFragment { Arguments = new Bundle() };
            detailFrag.Arguments.PutInt("id", id);            
            return detailFrag;
        }

        public int GetAnimalId
        {
            get { return Arguments.GetInt("id", 0); }
        }
    }
}