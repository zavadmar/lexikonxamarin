using System;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using LexiconZoo.Classes;
using Android.Text.Util;
using Android.Support.V7.Widget;

namespace LexiconZoo.Fragments
{
    public class AnimalZooFragment : Android.Support.V4.App.Fragment
    {
        private static string ANIMAL_ID = "animal_id";
        private Animal animal;
        
        public AnimalZooFragment() { }
        public static AnimalZooFragment newInstance(int animalId)
        {
            AnimalZooFragment fragment = new AnimalZooFragment();
            Bundle args = new Bundle();
            args.PutInt(ANIMAL_ID, animalId);            
            fragment.Arguments = args;

            return fragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (container == null)
            {
                // Currently in a layout without a container, so no reason to create our view.
                return null;
            }
            var scroller = new ScrollView(Activity);
            var layout = (LinearLayout)inflater.Inflate(Resource.Layout.AnimalTextDetail, container, false);
            var breedView = (TextView)layout.FindViewById<TextView>(Resource.Id.description);
            var breedHead = (TextView)layout.FindViewById<TextView>(Resource.Id.descriptionHead);
            breedHead.Text = Context.GetString(Resource.String.Breed);            
            var locationView = (TextView)layout.FindViewById<TextView>(Resource.Id.attraction);
            var locationHead = (TextView)layout.FindViewById<TextView>(Resource.Id.attractionHead);
            var locationCardLayout = (LinearLayout)layout.FindViewById<LinearLayout>(Resource.Id.attractionCardLayout);
            locationHead.Text = Context.GetString(Resource.String.Location);

            var mapBtn = new Button(Activity);
            mapBtn.Text = Context.GetString(Resource.String.ShowOnMap);
            mapBtn.Click += mapBtnClick;            
            locationCardLayout.AddView(mapBtn);

            var reproductionCard = (CardView)layout.FindViewById<CardView>(Resource.Id.reproductionCard);
            var projectCard = (CardView)layout.FindViewById<CardView>(Resource.Id.projectCard);

            projectCard.Visibility = ViewStates.Invisible;
            reproductionCard.Visibility = ViewStates.Invisible;
            animal = DBHandlerGetter.GetHandler().GetAnimalById(getAnimalId);            
            breedView.Text = animal.Breeding;
            locationView.Text = animal.LocalitiesTitle + "\n (" + animal.LocalitiesUrl+")";
            Linkify.AddLinks(locationView, MatchOptions.WebUrls);
            scroller.AddView(layout);
            return scroller;
        }

        private void mapBtnClick(object sender, EventArgs e)
        {
            Intent i = new Intent();
            i.SetClass(Activity, typeof(Activities.MapActivity));
            i.PutExtra(Activities.MapActivity.MARKER_TITLE,animal.LocalitiesTitle);
            StartActivity(i);
        }

        public int getAnimalId
        {
            get { return Arguments.GetInt(ANIMAL_ID, 1); }
        }
    }
}