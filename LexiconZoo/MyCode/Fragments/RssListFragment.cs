using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using LexiconZoo.Adapters;
using LexiconZoo.Activities;
using LexiconZoo.Classes;
using Android.Support.V4.App;

namespace LexiconZoo.Fragments
{
    public class RssListFragment : ListFragment
    {
        string currentId;
        bool isDualPane = false;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            var adapter = new RssListViewAdapter(Activity);
            ListAdapter = adapter;
            ListView.FastScrollEnabled = true;
            if (savedInstanceState != null)
            {
                currentId = savedInstanceState.GetString("currentId", "");
            }
            else
            {
                currentId = "";
            }
            var detailsFrame = Activity.FindViewById<View>(Resource.Id.details);
            // check if layout from Layout-w600dp is used
            isDualPane = detailsFrame != null && detailsFrame.Visibility == ViewStates.Visible;
            if (isDualPane)
            {
                ListView.ChoiceMode = ChoiceMode.Single;
                ShowDetails(currentId,1);
            }
        }

        private void ShowDetails(string rssId, int position)
        {
            this.currentId = rssId;

            if (isDualPane)
            {
                // open detail in second fragment
                ListView.SetItemChecked(position, true);

                var details = FragmentManager.FindFragmentById(Resource.Id.details) as RssDetailFragment;
                if (details == null || details.getRssId != rssId)
                {
                    // Make new fragment to show this selection.
                    details = RssDetailFragment.NewInstance(rssId);
                    // Execute a transaction, replacing any existing
                    // fragment with this one inside the frame.
                    var ft = FragmentManager.BeginTransaction();
                    ft.Replace(Resource.Id.details, details);
                    ft.SetTransition((int)Android.App.FragmentTransit.FragmentFade);
                    ft.Commit();
                }
            }
            // open detail in new activity
            else
            {               
                Intent i = new Intent();
                i.SetClass(Activity, typeof(RssDetailActivity));
                i.PutExtra(RssDetailActivity.RSS_ID, rssId);
                StartActivity(i);
            }
        }

        public override void OnListItemClick(ListView l, View v, int position, long id)
        {
            var stringId = DBHandlerGetter.GetHandler().GetRssByPosition(position)._id;
            ShowDetails(stringId, position);
        }

        public override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            outState.PutString("currentId", currentId);            
        }
    }
}