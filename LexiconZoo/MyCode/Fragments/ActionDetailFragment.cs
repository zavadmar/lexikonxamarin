using Android.OS;
using Android.Views;
using Android.Widget;
using LexiconZoo.Classes;
using Android.Text.Util;

namespace LexiconZoo.Fragments
{
    /// <summary>
    /// Fragment for detail of action
    /// </summary>
    public class ActionDetailFragment : Android.Support.V4.App.Fragment
    {
        private static string ACTION_ID = "action_id";
        private IDBHandler dbHandler;

        public ActionDetailFragment() {
            dbHandler = DBHandlerGetter.GetHandler();
        }
        /// <summary>
        /// Puts action id to fragment
        /// </summary>        
        public static ActionDetailFragment newInstance(string actionId)
        {
            ActionDetailFragment fragment = new ActionDetailFragment();
            Bundle args = new Bundle();
            args.PutString(ACTION_ID, actionId);            
            fragment.Arguments = args;
            return fragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (container == null)
            {
                return null;
            }
            var scroller = new ScrollView(Activity);
            var view = inflater.Inflate(Resource.Layout.RssDetail, container, false);
            var titleView = view.FindViewById<TextView>(Resource.Id.title);
            var dateView = view.FindViewById<TextView>(Resource.Id.date);
            var descView = view.FindViewById<TextView>(Resource.Id.description);
            var linkView = view.FindViewById<TextView>(Resource.Id.link);

            var action = dbHandler.GetActionById(getActionId);         
            titleView.Text = action.summary;
            dateView.Text = action.start.ToString("dd.MM.yyyy HH:mm") + " - " + action.end.ToString("dd.MM.yyyy  HH:mm");
            descView.Text = action.description;
            if (action.url != null && action.url != "")
            {
                linkView.Text = action.url;
                Linkify.AddLinks(linkView, MatchOptions.WebUrls);             
            }
            scroller.AddView(view);
            return scroller;        
        }

        private string getActionId
        {
            get { return Arguments.GetString(ACTION_ID, ""); }
        }
    }
}