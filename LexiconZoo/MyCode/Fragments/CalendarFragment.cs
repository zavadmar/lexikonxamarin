using System;
using System.Collections.Generic;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using LexiconZoo.Classes;
using LexiconZoo.Activities;
using Android.Support.V4.App;

namespace LexiconZoo.Fragments
{
    public class CalendarFragment : Fragment
    {
        private DateTime date;
        List<TableRow> weeks;
        IDBHandler dbHandler;        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.CalendarLayout, container, false);
            dbHandler = DBHandlerGetter.GetHandler();           
            date = new DateTime(year,month,1);
            setDays(view);                  
            return view;
        }
        /// <summary>
        /// Sets days in current month
        /// </summary>
        private void setDays(View view)
        {
            weeks = new List<TableRow>();
            var firstweek = view.FindViewById<TableRow>(Resource.Id.calendar_firstweek);
            weeks.Add(firstweek);
            weeks.Add(view.FindViewById<TableRow>(Resource.Id.calendar_secondweek));
            weeks.Add(view.FindViewById<TableRow>(Resource.Id.calendar_thirdweek));
            weeks.Add(view.FindViewById<TableRow>(Resource.Id.calendar_fourthweek));
            weeks.Add(view.FindViewById<TableRow>(Resource.Id.calendar_fifthtweek));
            weeks.Add(view.FindViewById<TableRow>(Resource.Id.calendar_sixthtweek));
            
            int day = 1;
            int child = (((int)date.DayOfWeek) - 1);
            if (child < 0) child = 6;

            for (int i = child; i< 7; i++)
            {
                ((TextView)firstweek.GetChildAt(i)).Text = day.ToString();
                var action = dbHandler.GetActionsByDate(new DateTimeOffset(year,month,day,0,0,0,new TimeSpan(0)));
                if (action.Count > 0)
                {
                    ((TextView)firstweek.GetChildAt(i)).SetBackgroundColor(Android.Graphics.Color.DarkCyan);
                    ((TextView)firstweek.GetChildAt(i)).Click += CalendarFragment_Click;
                }                
                day++;
            }
           
           foreach(var week in weeks)
            {
                if (week.Id == Resource.Id.calendar_firstweek) continue;
                for (int i = 0; ( i < 7) && DateTime.DaysInMonth(year,month) >= day; i++)
                {
                    ((TextView)week.GetChildAt(i)).Text = day.ToString();
                    var action = dbHandler.GetActionsByDate(new DateTimeOffset(year, month, day, 0, 0, 0, new TimeSpan(0)));
                    if (action.Count > 0)
                    {
                        ((TextView)week.GetChildAt(i)).SetBackgroundColor(Android.Graphics.Color.DarkCyan);
                        ((TextView)week.GetChildAt(i)).Click += CalendarFragment_Click;
                    }                                         
                    day++;
                }
            }                      
        }

        /// <summary>
        /// Click handler - shows dialog with action in clicked day
        /// </summary>        
        private void CalendarFragment_Click(object sender, EventArgs e)
        {
            var textV = sender as TextView;
            var day = int.Parse(textV.Text);

            // gets actions from DB
            var actions = dbHandler.GetActionsByDate(new DateTimeOffset(year, month, day, 0, 0, 0, new TimeSpan(0)));
            List<string> names = new List<string>();
            foreach (var action in actions)
            {
                names.Add(action.summary);
            }
            var namesArr = names.ToArray();

            // creates dialog with actions
            var builder = new Android.App.AlertDialog.Builder(Context);
            builder.SetTitle("Akce " + day + "." + month + "." + year);

            builder.SetItems(namesArr, (dialogSender, args) =>
            {
                var text = namesArr[args.Which];
                var action = dbHandler.GetActionBySummary(text);
                // open action detail
                var i = new Intent();                
                i.SetClass(Context, typeof(ActionDetailActivity));
                i.PutExtra(ActionDetailActivity.ACTION_ID, action._id);
                StartActivity(i);
            });

            builder.SetCancelable(true);
            var dialog = builder.Create();
            dialog.Show();
        }
              
        internal static CalendarFragment NewInstance(int month, int year)
        {
            var detailFrag = new CalendarFragment { Arguments = new Bundle() };
            detailFrag.Arguments.PutInt(monthS, month);
            detailFrag.Arguments.PutInt(yearS, year);
            return detailFrag;
        }

        public static string monthS = "month";
        public static string yearS = "year";

        public int month
        {
            get { return Arguments.GetInt(monthS, 1); }
        }
        public int year
        {
            get { return Arguments.GetInt(yearS, 2017); }
        }
    }
}