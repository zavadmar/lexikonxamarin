using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LexiconZoo.Adapters;
using LexiconZoo.Activities;
using Android.Support.V7.Widget;
using Android.Support.V4.App;

namespace LexiconZoo.Fragments
{
    public class AnimalListFragment : Fragment
    {
        ListView listView;
        int currentAnimalId;
        bool isDualPane = false;
        AnimalListViewAdapter adapter;
        private AnimalListViewAdapter.ViewType adapterViewType;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            this.HasOptionsMenu = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.AnimalList, container, false);
            adapterViewType = AnimalListViewAdapter.ViewType.CARDVIEW;
            int savedViewType = 0;
            if (savedInstanceState != null) savedViewType = savedInstanceState.GetInt("viewType");
            if (savedViewType != 0)
            {
                adapterViewType = (AnimalListViewAdapter.ViewType)savedViewType;
            }
            // get reference to the ListView in the layout
            listView = view.FindViewById<ListView>(Resource.Id.AnimalListView);
            // populate the listview with data  
            adapter = new AnimalListViewAdapter(Activity, adapterViewType);          
            listView.Adapter = adapter;        
            //sets item click event
            listView.ItemClick += OnListItemClick;  
            listView.FastScrollEnabled = true;         
            Activity.ActionBar.SetDisplayHomeAsUpEnabled(true);
            return view;
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.SearchMenu, menu);
            var searchView = menu.FindItem(Resource.Id.search).ActionView.JavaCast<Android.Widget.SearchView>();       
            searchView.QueryTextChange += InputSearchOnTextChanged;
        }

        /// <summary>
        /// Handle top menu item click
        /// </summary>
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                // enable search
                case Resource.Id.search:
                    item.ActionView.JavaCast<Android.Widget.SearchView>();
                    return true;
                // change views in list
                case Resource.Id.changeView:
                    if (adapterViewType == AnimalListViewAdapter.ViewType.CARDVIEW) adapterViewType = AnimalListViewAdapter.ViewType.LISTVIEW;
                    else adapterViewType = AnimalListViewAdapter.ViewType.CARDVIEW;
                    Activity.Recreate();
                    return true;
                case Android.Resource.Id.Home:
                    // app icon in action bar clicked; goto parent activity.
                    Activity.Finish();
                    return true;
                default:
                    return false;
            }            
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
      
            if (savedInstanceState != null)
            {
                currentAnimalId = savedInstanceState.GetInt("currentId", 0);
            }
            else
            {
                currentAnimalId = 0;
            }
            var detailsFrame = Activity.FindViewById<View>(Resource.Id.details);
            // check if layout from Layout-w600dp is used
            isDualPane = detailsFrame != null && detailsFrame.Visibility == ViewStates.Visible;
            if (isDualPane)
            {
                listView.ChoiceMode = ChoiceMode.Single;
                
                ShowDetails(currentAnimalId == 0 ? (int)adapter.GetItemId(0) : currentAnimalId);
            }
        }

        private void InputSearchOnTextChanged(object sender, Android.Widget.SearchView.QueryTextChangeEventArgs e)
        {
            adapter.Filter.InvokeFilter(e.NewText);
        }

        private void ShowDetails(int animalId)
        {
            this.currentAnimalId = animalId;
            if (isDualPane)
            {
                // open detail in second fragment
                listView.SetItemChecked(animalId, true);

                var details = FragmentManager.FindFragmentById(Resource.Id.details) as AnimalDetailTabsFragment;
                if (details == null || details.GetAnimalId != animalId)
                {
                    // Make new fragment to show this selection.
                    details = AnimalDetailTabsFragment.NewInstance(animalId);
                    // Execute a transaction, replacing any existing
                    // fragment with this one inside the frame.
                    var ft = FragmentManager.BeginTransaction();
                    ft.Replace(Resource.Id.details, details);
                    ft.SetTransition((int)Android.App.FragmentTransit.FragmentFade);
                    ft.Commit();
                }
            }
            else
            {               
                // open detail in new activity
                Intent i = new Intent();
                i.SetClass(Activity, typeof(AnimalDetailActivity));
                i.PutExtra("animalId", animalId);
                StartActivity(i);
            }
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var listView = sender as ListView;
            var animalId = listView.Adapter.GetItemId(e.Position);

            ShowDetails((int)animalId);
        }

        public override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            outState.PutInt("currentId", currentAnimalId);
            outState.PutInt("viewType", (int)adapterViewType);
        }
    }
}