using System.Collections.Generic;
using Android.Gms.Maps.Model;
using Android.Graphics;

namespace LexiconZoo.Classes
{
    /// <summary>
    /// Used to store created Zoo locations.
    /// </summary>
    public class MapLocation
    {
        public string Title { get; set; }
        public string FillColor { get; set; }
        public List<LatLng> Points { get; set; }
        public LatLng MarkerPoint { get; set; }

        //used by unit tests
        public class myPoint
        {
            public double latitude;
            public double longitude;
            public myPoint(double latitude, double longitude)
            {
                this.latitude = latitude;
                this.longitude = longitude;
            }
        }
        public List<myPoint> getMyPoints()
        {
            var mypoints = new List<myPoint>();
            foreach (var p in Points)
            {
                mypoints.Add(new myPoint(p.Latitude, p.Longitude));
            }
            return mypoints;
        }

        public myPoint getMyMarkerPoint()
        {
            return new myPoint(MarkerPoint.Latitude, MarkerPoint.Longitude);
        }

        public MapLocation(string title, string fillColor, LatLng markerPoint, List<LatLng> points)
        {
            this.Title = title;
            this.FillColor = fillColor;
            this.MarkerPoint = markerPoint;
            this.Points = points;
        }

        public MarkerOptions GetMarker()
        {
            MarkerOptions markerOpt1 = new MarkerOptions();
            markerOpt1.SetPosition(MarkerPoint);
            markerOpt1.SetTitle(Title);
            markerOpt1.Visible(false);
            return markerOpt1;
        }
        public PolygonOptions GetPolygon()
        {
            PolygonOptions option = new PolygonOptions();
            foreach (var p in Points)
            {
                option.Add(p);
            }            
            option.InvokeFillColor(Color.ParseColor(FillColor));
            option.InvokeStrokeWidth(0);
            option.Clickable(true);
            return option;
        }
    }
}