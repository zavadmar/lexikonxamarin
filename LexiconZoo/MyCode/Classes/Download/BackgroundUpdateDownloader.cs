﻿using Android.App;
using Android.Content;
using Android.OS;

namespace LexiconZoo.Classes
{
    /// <summary>
    /// Class form async download in background. Does not show any dialog.
    /// </summary>
    class BackgroundUpdateDownloader : AsyncTask<string, string, string>
    {
        private Context context;

        public BackgroundUpdateDownloader(Context c)
        {
            context = c;          
        }

        protected override void OnPreExecute()
        {     
            base.OnPreExecute();
        }

        protected override void OnPostExecute(Java.Lang.Object result)
        {            
            base.OnPostExecute(result);
        }

        public void downloadData()
        {
            var animalDownloader = new AnimalDownloader();
            animalDownloader.DownloadData();
            var actionDownloader = new ActionsDownloader();
            actionDownloader.DownloadData();
            var rssDownloader = new RssDownloader();
            rssDownloader.DownloadData();
        }       

        protected override string RunInBackground(params string[] @params)
        {
            downloadData();
            return null;
        }

    }
}
