﻿using System;
using System.Linq;

namespace LexiconZoo.Classes
{
    class AnimalDownloader
    {       
        public bool DownloadData()
        {
            var dbHandler = DBHandlerGetter.GetHandler();
            var restService = new RestService();
            var animals = restService.GetAllAnimals();
            if (animals != null)
            {
                dbHandler.UpdateAnimals(animals);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
