﻿using Android.App;
using Android.Content;
using Android.OS;

namespace LexiconZoo.Classes
{
    /// <summary>
    /// Class for initial async download data. Show progress dialog on screen.
    /// </summary>
    class CompleteDownloader : AsyncTask<string, string, string>
    {
        private ProgressDialog pDialog;
        private Context context;

        public bool animalSuccess;
        public bool actionSuccess;
        public bool rssSuccess;
        public CompleteDownloader(Context c)
        {
            context = c;          
        }

        protected override void OnPreExecute()
        {
            pDialog = new ProgressDialog(context);
            pDialog.SetMessage("Stahuji data...");
            pDialog.Indeterminate = false;
            pDialog.Max = 3;
            pDialog.Progress = 1;
            pDialog.SetProgressStyle(ProgressDialogStyle.Horizontal);
            pDialog.SetCancelable(false);
            pDialog.Show();            
            base.OnPreExecute();
        }

        protected override void OnPostExecute(Java.Lang.Object result)
        {
            if (pDialog != null && pDialog.IsShowing)
                pDialog.Dismiss();
                     
            base.OnPostExecute(result);
        }

        private void downloadData()
        {
            pDialog.Progress = 0;
            var animalDownloader = new AnimalDownloader();
            animalSuccess = animalDownloader.DownloadData();
            pDialog.Progress = 1;
            var actionDownloader = new ActionsDownloader();
            actionSuccess = actionDownloader.DownloadData();
            pDialog.Progress = 2;
            var rssDownloader = new RssDownloader();
            rssSuccess = rssDownloader.DownloadData();
            pDialog.Progress = 3;
        }       

        protected override string RunInBackground(params string[] @params)
        {
            downloadData();
            return null;
        }

    }
}
