﻿using System;
using System.Linq;


namespace LexiconZoo.Classes
{
    class ActionsDownloader
    {              
        public bool DownloadData()
        {
            var dbHandler = DBHandlerGetter.GetHandler();       
            var restService = new RestService();          
            var actions = restService.GetAllActions();
            if (actions != null)
            {
                dbHandler.UpdateActions(actions);
                return true;
            }
            else
            {
                return false;
            }
                
        }
    }
}
