﻿using System;
using System.Linq;

namespace LexiconZoo.Classes
{
    class RssDownloader 
    {     
        public bool DownloadData()
        {
            var dbHandler = DBHandlerGetter.GetHandler();
            var restService = new RestService();          
            var rss = restService.GetAllRss();
            if (rss != null)
            {
                dbHandler.UpdateRss(rss);
                return true;
            }
            else
            {
                return false;
            }
                
        }             
    }
}
