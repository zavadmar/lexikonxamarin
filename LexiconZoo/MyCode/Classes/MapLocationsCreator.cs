using System.Collections.Generic;
using Android.Gms.Maps.Model;

namespace LexiconZoo.Classes
{
    /// <summary>
    /// Creates Zoo locations.
    /// </summary>
    public class MapLocationsCreator 
    {
        public static List<MapLocation> CreateLocations()
        {
            var list = new List<MapLocation>();
            list.Add(createAfrickyDum());
            list.Add(createUdoliSlonu());
            list.Add(createLachtani());
            list.Add(createPavilonHrochu());
            list.Add(createAfrikaZblizka());
            list.Add(createSeverskyLes());
            list.Add(createPlane());
            list.Add(createTucnaci());
            list.Add(createIndoneskaDzungle());
            list.Add(createDravci());
            list.Add(createPapousci());
            list.Add(createOstrovLemuru());
            list.Add(createCambal());
            list.Add(createTerarium());
            list.Add(createPavilonSelem());
            list.Add(createPavilonGoril());
            list.Add(createVelemlokarium());
            list.Add(createZelvy());
            list.Add(createSecuan());
            list.Add(createPtaciMokrady());
            list.Add(createVoliery());
            list.Add(createHorstiKopytnici());
            list.Add(createDetskaZoo());
            list.Add(createPrirodaKolem());
            list.Add(createOstrovy());
            return list;
        }

        protected static MapLocation createAfrickyDum()
        {
            List<LatLng> points = new List<LatLng>();            
            points.Add(new LatLng(50.119466, 14.407191));
            points.Add(new LatLng(50.119559, 14.407481));
            points.Add(new LatLng(50.119612, 14.407521));
            points.Add(new LatLng(50.119554, 14.407811));
            points.Add(new LatLng(50.119652, 14.407902));
            points.Add(new LatLng(50.119836, 14.407266));
            points.Add(new LatLng(50.119581, 14.407078));                        
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.africkydum), "#A0f4d142", new LatLng(50.119686, 14.407458), points);            
        }

        protected static MapLocation createUdoliSlonu()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.120201, 14.403710));
            points.Add(new LatLng(50.120026, 14.403558));
            points.Add(new LatLng(50.119899, 14.403901));
            points.Add(new LatLng(50.119271, 14.404731));
            points.Add(new LatLng(50.118827, 14.405305));
            points.Add(new LatLng(50.118466, 14.406426));
            points.Add(new LatLng(50.118600, 14.407027));
            points.Add(new LatLng(50.118782, 14.407231));
            points.Add(new LatLng(50.118923, 14.407263));
            points.Add(new LatLng(50.119068, 14.407179));
            points.Add(new LatLng(50.119281, 14.406567));
            points.Add(new LatLng(50.119367, 14.406599));            
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.udolislonu), "#A0f49841", new LatLng(50.118906, 14.406447), points);
        }
        protected static MapLocation createLachtani()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116213, 14.411002));
            points.Add(new LatLng(50.115766, 14.410814));
            points.Add(new LatLng(50.115886, 14.410059));
            points.Add(new LatLng(50.116314, 14.410473));
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.lachtani), "#A04286f4", new LatLng(50.116053, 14.410675), points);
        }
        protected static MapLocation createTucnaci()
        {
            List<LatLng> points = new List<LatLng>();            
            points.Add(new LatLng(50.116314, 14.410473));
            points.Add(new LatLng(50.115886, 14.410059));
            points.Add(new LatLng(50.115936, 14.409751));
            points.Add(new LatLng(50.116087, 14.409933));
            points.Add(new LatLng(50.116225, 14.410038));
            points.Add(new LatLng(50.116383, 14.410100));
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.tucnaci), "#A080d3f2", new LatLng(50.116110, 14.410092), points);
        }
        protected static MapLocation createPavilonHrochu()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.118959, 14.407922));
            points.Add(new LatLng(50.118785, 14.408736));
            points.Add(new LatLng(50.118322, 14.408193));
            points.Add(new LatLng(50.118315, 14.407540));
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.hrosi), "#A0f4e842", new LatLng(50.118449, 14.408011), points);
        }
        protected static MapLocation createAfrikaZblizka()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.118679, 14.409197));
            points.Add(new LatLng(50.118713, 14.409791));
            points.Add(new LatLng(50.118235, 14.409763));
            points.Add(new LatLng(50.118219, 14.409234));
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.afrika), "#A0d8320d", new LatLng(50.118373, 14.409491), points);
        }

        protected static MapLocation createSeverskyLes()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.120026, 14.403558));// border with udoli slonu
            points.Add(new LatLng(50.120201, 14.403710));// border with udoli slonu
            points.Add(new LatLng(50.120146, 14.403562));
            points.Add(new LatLng(50.120146, 14.403326));
            points.Add(new LatLng(50.120170, 14.402827));
            points.Add(new LatLng(50.120115, 14.402773));
            points.Add(new LatLng(50.119847, 14.401160));
            points.Add(new LatLng(50.119782, 14.401153));
            points.Add(new LatLng(50.119603, 14.400976));
            points.Add(new LatLng(50.119300, 14.400794)); // border with skalni masiv
            points.Add(new LatLng(50.118691, 14.401168));// border with skalni masiv
            points.Add(new LatLng(50.118466, 14.401987));// border with skalni masiv
            points.Add(new LatLng(50.118625, 14.402020));// border with plane
            points.Add(new LatLng(50.118845, 14.402237)); // border with plane
            points.Add(new LatLng(50.119089, 14.402795));// border with plane
            points.Add(new LatLng(50.119172, 14.403246));// border with plane
            points.Add(new LatLng(50.119108, 14.403623));// border with plane
            points.Add(new LatLng(50.118948, 14.403966));// border with plane
            points.Add(new LatLng(50.118513, 14.404543));// border with plane a horni zoo
            points.Add(new LatLng(50.119271, 14.404731));// border with hotni zoo a sloni
            points.Add(new LatLng(50.119899, 14.403901));
          
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.severskyles), "#A01f6b2f", new LatLng(50.119237, 14.402534), points);
        }

        protected static MapLocation createPlane()
        {
            List<LatLng> points = new List<LatLng>();
            
            points.Add(new LatLng(50.118625, 14.402020));  // border with seversky les
            points.Add(new LatLng(50.118845, 14.402237)); //border with seversky les
            points.Add(new LatLng(50.119089, 14.402795));  // border with seversky les
            points.Add(new LatLng(50.119172, 14.403246));  // border with seversky les
            points.Add(new LatLng(50.119108, 14.403623));  // border with seversky les
            points.Add(new LatLng(50.118948, 14.403966));  // border with seversky les
            points.Add(new LatLng(50.118513, 14.404543));  // border with seversky les a horni zoo
            points.Add(new LatLng(50.118152, 14.405156));  // border with horni zoo
            points.Add(new LatLng(50.117642, 14.405910));  // border with horni zoo
            points.Add(new LatLng(50.117568, 14.405969));  // border with horni zoo a papousci
            points.Add(new LatLng(50.117446, 14.405762));  // border with papousci
            points.Add(new LatLng(50.117439, 14.405564));
            points.Add(new LatLng(50.117518, 14.404279));
            points.Add(new LatLng(50.117936, 14.403083));
            points.Add(new LatLng(50.118598, 14.402021));
            
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.plane), "#A0aee524", new LatLng(50.118190, 14.403795), points);
        }
        protected static MapLocation createIndoneskaDzungle()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.117209, 14.410407)); 
            points.Add(new LatLng(50.117751, 14.410552));                     
            points.Add(new LatLng(50.117890, 14.409583));
            points.Add(new LatLng(50.117347, 14.409905));
            points.Add(new LatLng(50.117324, 14.410050));
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.dzungle), "#A0ff4242", new LatLng(50.117547, 14.410144), points);
        }

        protected static MapLocation createDravci()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.115886, 14.410059));
            points.Add(new LatLng(50.115936, 14.409751));
            points.Add(new LatLng(50.115750, 14.409612));
            points.Add(new LatLng(50.115489, 14.409140));
            points.Add(new LatLng(50.115461, 14.408807));
            points.Add(new LatLng(50.115365, 14.408599));
            points.Add(new LatLng(50.115190, 14.409152));
            points.Add(new LatLng(50.115663, 14.409989));
            points.Add(new LatLng(50.115663, 14.409989));
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.dravci), "#A04a51db", new LatLng(50.115703, 14.409673), points);
        }
        protected static MapLocation createPapousci()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.117327, 14.409041));
            points.Add(new LatLng(50.117523, 14.409017));
            points.Add(new LatLng(50.117461, 14.406179));
            points.Add(new LatLng(50.117569, 14.406195));

            points.Add(new LatLng(50.117568, 14.405969));
            points.Add(new LatLng(50.117446, 14.405762));

            points.Add(new LatLng(50.117372, 14.405889));
            points.Add(new LatLng(50.117332, 14.406337));            
            points.Add(new LatLng(50.117349, 14.407004));
            points.Add(new LatLng(50.117359, 14.408047));
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.papousci), "#A0fff951", new LatLng(50.117376, 14.407167), points);
        }
        protected static MapLocation createOstrovLemuru()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116387, 14.410099));
            points.Add(new LatLng(50.116600, 14.410053));
            points.Add(new LatLng(50.116910, 14.410139));
            points.Add(new LatLng(50.116953, 14.409659));

            points.Add(new LatLng(50.116704, 14.409495));
            points.Add(new LatLng(50.116704, 14.409323));

            points.Add(new LatLng(50.116568, 14.409251));
           
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.lemuri), "#A02f73ef", new LatLng(50.116786, 14.409820), points);
        }

        protected static MapLocation createCambal()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116177, 14.407589));
            points.Add(new LatLng(50.116120, 14.407970));
            points.Add(new LatLng(50.116233, 14.408012));
            points.Add(new LatLng(50.116300, 14.407626));

            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.cambal), "#C0003eaf", new LatLng(50.116185, 14.407779), points);
        }
        protected static MapLocation createTerarium()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116646, 14.407784));
            points.Add(new LatLng(50.116407, 14.407679));
            points.Add(new LatLng(50.116324, 14.408079));
            points.Add(new LatLng(50.116597, 14.408151));

            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.terarium), "#A00fc43f", new LatLng(50.116496, 14.407912), points);
        }
        protected static MapLocation createPavilonGoril()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116048, 14.404940));
            points.Add(new LatLng(50.115852, 14.405533));
            points.Add(new LatLng(50.115195, 14.405098));
            points.Add(new LatLng(50.115362, 14.404506)); //selmy
            points.Add(new LatLng(50.115639, 14.404652));//selmy
            points.Add(new LatLng(50.115732, 14.404583));//selmy
            points.Add(new LatLng(50.116019, 14.404787));//selmy

            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.gorily), "#A000d82f", new LatLng(50.115450, 14.404884), points);
        }

        protected static MapLocation createPavilonSelem()
        {
            List<LatLng> points = new List<LatLng>();            
            points.Add(new LatLng(50.115362, 14.404506)); 
            points.Add(new LatLng(50.115639, 14.404652));
            points.Add(new LatLng(50.115732, 14.404583));
            points.Add(new LatLng(50.116019, 14.404787));
            points.Add(new LatLng(50.116181, 14.404470));  //velemlok

            points.Add(new LatLng(50.116424, 14.404162));  //mokrady, velemlok
            points.Add(new LatLng(50.116677, 14.403245));  //mokrady
            points.Add(new LatLng(50.116675, 14.403124));  //mokrady
            points.Add(new LatLng(50.116613, 14.402947));  //mokrady
            points.Add(new LatLng(50.116587, 14.402681));  //mokrady
            points.Add(new LatLng(50.116265, 14.402131));  //mokrady

            points.Add(new LatLng(50.115967, 14.402488));
            points.Add(new LatLng(50.115821, 14.402764));
            points.Add(new LatLng(50.115250, 14.404459));

            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.selmy), "#A031e060", new LatLng(50.116185, 14.403558), points);
        }

        protected static MapLocation createVelemlokarium()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116181, 14.404470));  
            points.Add(new LatLng(50.116424, 14.404162));
            points.Add(new LatLng(50.116574, 14.404805));
            points.Add(new LatLng(50.116269, 14.404644));            

            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.velemlokarium), "#A004d6b6", new LatLng(50.116400, 14.404502), points);
        }
        protected static MapLocation createZelvy()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116181, 14.404470));
            points.Add(new LatLng(50.116269, 14.404644));            
            points.Add(new LatLng(50.116574, 14.404805));

            points.Add(new LatLng(50.116712, 14.404869));
            points.Add(new LatLng(50.116714, 14.405411));
            points.Add(new LatLng(50.116348, 14.405494));
            points.Add(new LatLng(50.116334, 14.405357));
            points.Add(new LatLng(50.116031, 14.404898));
            points.Add(new LatLng(50.116022, 14.404791));

            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.zelvy), "#A002821e", new LatLng(50.116474, 14.405188), points);
        }

        protected static MapLocation createPtaciMokrady()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116712, 14.404869));
            points.Add(new LatLng(50.116574, 14.404805));
            points.Add(new LatLng(50.116424, 14.404162));  
            points.Add(new LatLng(50.116677, 14.403245));  
            points.Add(new LatLng(50.116675, 14.403124)); 
            points.Add(new LatLng(50.116613, 14.402947));  
            points.Add(new LatLng(50.116587, 14.402681));  
            points.Add(new LatLng(50.116265, 14.402131));
            points.Add(new LatLng(50.117258, 14.400964));
            points.Add(new LatLng(50.117301, 14.400964));
            points.Add(new LatLng(50.117728, 14.401605));
            points.Add(new LatLng(50.116989, 14.403286));
            points.Add(new LatLng(50.116839, 14.403777));

            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.mokrady), "#A0b6ce16", new LatLng(50.116983, 14.402490), points);
        }

        protected static MapLocation createSecuan()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116311, 14.405958));
            points.Add(new LatLng(50.116299, 14.406347));  // ostrovy
            points.Add(new LatLng(50.116590, 14.406511));  // ostrovy, voliery
            points.Add(new LatLng(50.116654, 14.405996));  //voliery
            
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.secuan), "#A0cebe15", new LatLng(50.116501, 14.406243), points);
        }

        protected static MapLocation createVoliery()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116931, 14.407172));
            points.Add(new LatLng(50.117183, 14.407092));
            points.Add(new LatLng(50.117197, 14.407751));
            points.Add(new LatLng(50.116973, 14.407865));
            points.Add(new LatLng(50.116854, 14.406737));  // ostrovy
            points.Add(new LatLng(50.116590, 14.406511));  // ostrovy
            points.Add(new LatLng(50.116654, 14.405996));
            points.Add(new LatLng(50.116553, 14.405752));
            points.Add(new LatLng(50.116545, 14.405489));
            points.Add(new LatLng(50.116708, 14.405435));

            points.Add(new LatLng(50.116764, 14.404688));
            points.Add(new LatLng(50.117106, 14.404663));
            points.Add(new LatLng(50.117004, 14.405807));
            points.Add(new LatLng(50.116843, 14.406032));            
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.ptacisvet), "#A07c7200", new LatLng(50.116745, 14.406110), points);
        }

        protected static MapLocation createPrirodaKolem()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.114693, 14.406488));

            points.Add(new LatLng(50.115180, 14.407237));
            points.Add(new LatLng(50.115323, 14.406812));
            points.Add(new LatLng(50.115311, 14.406606));
            points.Add(new LatLng(50.115168, 14.406429));  
            points.Add(new LatLng(50.114807, 14.406029));  
            
            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.prirodakolemnas), "#A017c683", new LatLng(50.115162, 14.406859), points);
        }

        protected static MapLocation createDetskaZoo()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.115180, 14.407237));
            points.Add(new LatLng(50.115323, 14.406812));
            points.Add(new LatLng(50.115311, 14.406606));
            points.Add(new LatLng(50.115168, 14.406429));
            points.Add(new LatLng(50.114807, 14.406029));
            points.Add(new LatLng(50.115060, 14.405609));
            points.Add(new LatLng(50.115423, 14.405748));
            points.Add(new LatLng(50.115955, 14.406327));
            points.Add(new LatLng(50.116192, 14.406481));
            points.Add(new LatLng(50.115485, 14.407617));
            points.Add(new LatLng(50.115155, 14.407412));

            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.detskazoo), "#A079c8e5", new LatLng(50.115581, 14.406579), points);
        }

        protected static MapLocation createOstrovy()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.115201, 14.407543));
            points.Add(new LatLng(50.115097, 14.408191));
            points.Add(new LatLng(50.115475, 14.408652));
            points.Add(new LatLng(50.115571, 14.409205));
            points.Add(new LatLng(50.115760, 14.409581));
            points.Add(new LatLng(50.115922, 14.409678));
            points.Add(new LatLng(50.116211, 14.408042));
            points.Add(new LatLng(50.116108, 14.407988));
            points.Add(new LatLng(50.116179, 14.407570));
            points.Add(new LatLng(50.116296, 14.407559));
            points.Add(new LatLng(50.116857, 14.407144));
            points.Add(new LatLng(50.116854, 14.406737));
            points.Add(new LatLng(50.116590, 14.406511));
            points.Add(new LatLng(50.116299, 14.406347));

            points.Add(new LatLng(50.116320, 14.406486));
            points.Add(new LatLng(50.116206, 14.406564));
            points.Add(new LatLng(50.115547, 14.407685));            

            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.vodnisvet), "#A02d92ff", new LatLng(50.115810, 14.407881), points);
        }

        protected static MapLocation createHorstiKopytnici()
        {
            List<LatLng> points = new List<LatLng>();
            points.Add(new LatLng(50.116768, 14.404629));
            points.Add(new LatLng(50.117086, 14.404636));
            points.Add(new LatLng(50.117264, 14.403620));
            points.Add(new LatLng(50.118585, 14.401118));
            points.Add(new LatLng(50.118265, 14.400712));
            points.Add(new LatLng(50.117737, 14.401621));
            points.Add(new LatLng(50.117009, 14.403301));
            points.Add(new LatLng(50.116849, 14.403883));
            points.Add(new LatLng(50.116720, 14.405036));        

            return new MapLocation(Android.App.Application.Context.GetString(Resource.String.kopytnici), "#A075520e", new LatLng(50.117043, 14.403859), points);
        }
    }   
}