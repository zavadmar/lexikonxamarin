using System;
using System.Collections.Generic;
using System.Linq;

namespace LexiconZoo.Classes
{
    class RealmDBHandler : IDBHandler
    {
        Realms.Realm realm;
        const int versionId = 1;
        public RealmDBHandler()
        {
            realm = Realms.Realm.GetInstance();
        }

        public Animal GetAnimalById(int id) {          
            return realm.Find<Animal>(id);
        }

        public int GetAnimalCnt()
        {
            return realm.All<Animal>().Count();
        }

        public void UpdateAnimals(List<Animal> animals)
        {          
            if (animals != null)
                {
                realm.Write(() =>
                {
                    foreach (var animal in animals)
                    {
                        if (animal.deleted)
                        {
                            var dbAnimal = GetAnimalById(animal._id);
                            if (dbAnimal != null) realm.Remove(dbAnimal);
                        }
                        else
                        {
                            realm.Add(animal, true);
                        }
                    }
                });
            }
        }

        public int GetRssCnt()
        {
            return realm.All<Rss>().Count();
        }

        public void UpdateRss(List<Rss> rssList)
        {
            if (rssList != null)
            {
                realm.Write(() =>
                {
                    foreach (var rss in rssList)
                    {
                        if (rss.deleted)
                        {
                            var dbRss = GetRssById(rss._id);
                            if (dbRss != null) realm.Remove(dbRss);
                        }
                        else
                        {
                            realm.Add(rss, true);
                        }                     
                    }
                });
            }
        }
        public int GetActionsCnt()
        {
            return realm.All<Action>().Count();
        }
        public void UpdateActions(List<Action> actionList)
        {
            if (actionList != null)
            {
                realm.Write(() =>
                {
                    foreach (var action in actionList)
                    {
                        if (action.deleted)
                        {
                            var dbAction = GetActionById(action._id);
                            if (dbAction != null) realm.Remove(dbAction);
                        }
                        else
                        {
                            realm.Add(action, true);
                        }
                    }
                });
            }
        }

        public Rss GetRssById(string id)
        {
            return realm.Find<Rss>(id);
        }

        public Action GetActionById(string id)
        {
            return realm.Find<Action>(id);
        }

        public IQueryable<Animal> GetAllAnimals()
        {
            return realm.All<Animal>().OrderBy(x => x.Title);
        }

        public IQueryable<Rss> GetAllRss()
        {
            return realm.All<Rss>().OrderByDescending(x => x.date);
        }

        public IQueryable<Action> GetAllActions()
        {
            return realm.All<Action>().OrderBy(x => x.start);
        }

        public Rss GetRssByPosition(int position)
        {
            return realm.All<Rss>().OrderByDescending(x => x.date).ElementAt(position);
        }

        public List<Action> GetAllActionsInMonth(int year, int month)
        {
            var zero = month > 9 ? "0" : "";
            return realm.All<Action>().Where(x => (x.start.Month == month && x.start.Year == year) || (x.end.Month == month && x.end.Year == year)).ToList();
        }

        public List<Action> GetActionsByDate(DateTimeOffset dateTime)
        {
            var actions = realm.All<Action>();
            var midnightDate = dateTime.AddDays(1);          
            return actions.Where(x => (x.end >= dateTime && x.start < midnightDate)).ToList();
        }

        public Action GetActionBySummary(string text)
        {
            return realm.All<Action>().Where(x => x.summary == text).First();
        }     

        public DateTime? GetAnimalsLastDate()
        {
            if (getVersion().animalLastDate!= null)
                return getVersion().animalLastDate.Value.DateTime;
            else
                return null;
        }

        public DateTime? GetActionsLastDate()
        {
            if (getVersion().actionLastDate != null)
                return getVersion().actionLastDate.Value.DateTime;
            else
                return null;         
        }

        public DateTime? GetRssLastDate()
        {
            if  (getVersion().rssLastDate != null)
                return getVersion().rssLastDate.Value.DateTime;
            else
                return null;
        }

        public void WriteAnimalsLastDate(DateTime date)
        {            
            realm.Write(() =>
            {
                var version = getVersion();
                version.animalLastDate = date;
                realm.Add(version, true);                
            });
        }

        public void WriteActionsLastDate(DateTime date)
        {                        
            realm.Write(() =>
            {
                var version = getVersion();
                version.actionLastDate = date;
                realm.Add(version, true);
            });
        }

        public void WriteRssLastDate(DateTime date)
        {                       
            realm.Write(() =>
            {
                var version = getVersion();
                version.rssLastDate = date;
                realm.Add(version, true);
            });
        }

        /// <summary>
        /// Returns Versions database object.
        /// </summary>
        /// <returns></returns>
        protected Versions getVersion()
        {
            var version = realm.Find<Versions>(versionId);
            if (version == null)
            {
                version = new Versions();
                version._id = versionId;
            }
            return version;
        }
    }
}