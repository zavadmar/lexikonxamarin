using System;
using System.Collections.Generic;
using System.Linq;

namespace LexiconZoo.Classes
{
    /// <summary>
    /// Interface for communication with DB.
    /// </summary>
    interface IDBHandler
    {
        /// <summary>
        /// Find animal by given id and return it.
        /// </summary>
        /// <param name="id">Id of animal for search.</param>
        /// <returns>Found animal object.</returns>
        Animal GetAnimalById(int id);

        /// <summary>
        /// Returns all animals as IQueryable collection.
        /// </summary>
        IQueryable<Animal> GetAllAnimals();

        /// <summary>
        /// Returns all rss as as IQueryable collection.
        /// </summary>
        /// <returns></returns>
        IQueryable<Rss> GetAllRss();

        /// <summary>
        /// Returns all actions as as IQueryable collection.
        /// </summary>
        IQueryable<Action> GetAllActions();

        /// <summary>
        /// Returns animals count in DB.
        /// </summary>
        int GetAnimalCnt();

        /// <summary>
        /// Update Animals in DB from given List. If animal has "deleted" property equal true, then delete it from DB, else insert/update it.
        /// </summary>
        void UpdateAnimals(List<Animal> animals);

        /// <summary>
        /// Find Rss by given id in database.
        /// </summary>
        Rss GetRssById(string id);

        /// <summary>
        /// Returns Rss count in DB.
        /// </summary>
        int GetRssCnt();

        /// <summary>
        /// Update Rss in DB from given List. If animal has "deleted" property equal true, then delete it from DB, else insert/update it.
        /// </summary>
        void UpdateRss(List<Rss> rssList);

        /// <summary>
        /// Find Action by given id in database.
        /// </summary>
        Action GetActionById(string id);

        /// <summary>
        /// Returns Action count in DB.
        /// </summary>
        int GetActionsCnt();

        /// <summary>
        /// Update Rss in DB from given List. If animal has "deleted" property equal true, then delete it from DB, else insert/update it.
        /// </summary>
        void UpdateActions(List<Action> actions);

        /// <summary>
        /// Returns Rss in given position.
        /// </summary>
        Rss GetRssByPosition(int position);

        /// <summary>
        /// Returns all action in given month in given year.
        /// </summary>
        List<Action> GetAllActionsInMonth(int year, int month);

        /// <summary>
        /// Find action by given date.
        /// </summary>        
        List<Action> GetActionsByDate(DateTimeOffset dateTime);

        /// <summary>
        /// Fing action by given summary.
        /// </summary>
        Action GetActionBySummary(string text);

        /// <summary>
        /// Return animals last update date. Used by "if-modified-since" HTTP header.
        /// </summary>
        DateTime? GetAnimalsLastDate();

        /// <summary>
        /// Return action last update date. Used by "if-modified-since" HTTP header.
        /// </summary>
        DateTime? GetActionsLastDate();

        /// <summary>
        /// Return rss last update date. Used by "if-modified-since" HTTP header.
        /// </summary>
        DateTime? GetRssLastDate();

        /// <summary>
        /// Write animals last update date from "last-modified" HTTP header.
        /// </summary>
        void WriteAnimalsLastDate(DateTime date);

        /// <summary>
        /// Write animals last update date from "last-modified" HTTP header.
        /// </summary>
        void WriteActionsLastDate(DateTime date);

        /// <summary>
        /// Write animals last update date from "last-modified" HTTP header.
        /// </summary>
        void WriteRssLastDate(DateTime date);
    }
}