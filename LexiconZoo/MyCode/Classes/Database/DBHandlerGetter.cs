namespace LexiconZoo.Classes
{
    /// <summary>
    /// Return implementation of IDBHandler.
    /// </summary>
    static class DBHandlerGetter
    {
        public static IDBHandler GetHandler()
        {
            return new RealmDBHandler();
        }
    }
}