﻿using Android.Media;
using Realms;
using System;

/// <summary>
/// Realm database model.
/// </summary>
namespace LexiconZoo
{
    public class Animal : RealmObject
    {
        [PrimaryKey]
        public int _id { get; set; }
        public string Title { get; set; }
        public string LatinTitle { get; set; }
        public string ImageAlt { get; set; }
        public string ImageUrl { get; set; }
        public string Continent { get; set; }
        public string Class { get; set; }
        public string Order { get; set; }
        public string SpreadNote { get; set; }
        public string Biotop { get; set; }
        public string BiotopesNotes { get; set; }
        public string Food { get; set; }
        public string FoodNotes { get; set; }
        public string Proportions { get; set; }
        public string Reproduction { get; set; }
        public string ProjectsNote { get; set; }
        public string Attractions { get; set; }
        public string Breeding { get; set; }
        public string LocalitiesTitle { get; set; }
        public string LocalitiesUrl { get; set; }
        public string Description { get; set; }
        public string ImgPath { get; set; }
        public DateTimeOffset updated_at { get; set; }
        public DateTimeOffset last_visit { get; set; }
        public Boolean deleted { get; set; }
        [Ignored]
        public Image Img { get; set; }
    }

    public class Rss : RealmObject
    {
        [PrimaryKey]
        public string _id { get; set; }
        public string title { get; set; }
        public string link { get; set; }
        public string description { get; set; }
        public string date { get; set; }     
        public DateTimeOffset updated_at { get; set; }
        public DateTimeOffset last_visit { get; set; }
        public Boolean deleted { get; set; }
    }

    public class Action : RealmObject
    {
        [PrimaryKey]
        public string _id { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string summary { get; set; }
        public DateTimeOffset start { get; set; }
        public DateTimeOffset end { get; set; }
        public DateTimeOffset updated_at { get; set; }
        public DateTimeOffset last_visit { get; set; }
        public Boolean deleted { get; set; }
    }

    /// <summary>
    /// Contains dates from "last-modified" HTTP header.
    /// </summary>
    public class Versions : RealmObject
    {
        [PrimaryKey]
        public int _id { get; set; }
        public DateTimeOffset? animalLastDate { get; set; }
        public DateTimeOffset? actionLastDate { get; set; }
        public DateTimeOffset? rssLastDate { get; set; }
    }
}