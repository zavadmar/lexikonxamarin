﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using LexiconZoo.Classes;
using System.Linq;

namespace LexiconZoo
{
    /// <summary>
    /// Used for download data from REST API.
    /// </summary>
    public class RestService
    {
        readonly String animalsRestUrl;
        readonly String rssRestUrl;
        readonly String actionsRestUrl;
        readonly int timeout;
        public RestService()
        {
            //restUrl = "http://10.0.2.2:1337/api/animals";  // localhost

            animalsRestUrl = "https://agile-chamber-32011.herokuapp.com/api/animals";
            rssRestUrl = "https://agile-chamber-32011.herokuapp.com/api/rss";
            actionsRestUrl = "https://agile-chamber-32011.herokuapp.com/api/actions";
            timeout = 1000;
        }
       
        public List<Animal> GetAllAnimals()
        {
            //var client = new RestClient(animalsRestUrl);

            //var request = new RestRequest();
            //request.AddParameter("Timeout", timeout);
            //// set "If-Modified-Since" header
            //var dbHandler = DBHandlerGetter.GetHandler();
            //var lastUpdateDate = dbHandler.GetAnimalsLastDate();
            //if (lastUpdateDate != null)
            //{
            //    request.AddHeader("If-Modified-Since", lastUpdateDate.ToString());
            //}

            // execute request
            //var response = client.Execute<List<Animal>>(request);            

            // find last modified date 
            var dbHandler = DBHandlerGetter.GetHandler();
            var lastUpdateDate = dbHandler.GetAnimalsLastDate();

            // send request
            var response = RequestAnimals(lastUpdateDate);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // get "Last-Modified" header and write to DB
                if (response.Headers.Any(x => x.Name == "Last-Modified"))
                {
                    var lastModify = DateTime.Parse(response.Headers.First(x => x.Name == "Last-Modified").Value.ToString());
                    dbHandler.WriteAnimalsLastDate(lastModify);
                }
                return response.Data;
            }                
            else
                return null;
        }

        public IRestResponse<List<Animal>> RequestAnimals(DateTime? lastModified = null)
        {
            var client = new RestClient(animalsRestUrl);
            var request = new RestRequest();
            request.AddParameter("Timeout", timeout);
            // set "If-Modified-Since" header
            if (lastModified != null)
            {
                request.AddHeader("If-Modified-Since", lastModified.ToString());
            }
            return client.Execute<List<Animal>>(request);
        }
    
        public List<Rss> GetAllRss()
        {
            // find last modified date 
            var dbHandler = DBHandlerGetter.GetHandler();
            var date = dbHandler.GetRssLastDate();           

            // execute request
            var response = RequestRss(date);
           
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // get "Last-Modified" header and write to DB
                if (response.Headers.Any(x => x.Name == "Last-Modified"))
                {
                    var lastModify = DateTime.Parse(response.Headers.First(x => x.Name == "Last-Modified").Value.ToString());
                    dbHandler.WriteRssLastDate(lastModify);
                }
                return response.Data;
            }
                
            else
                return null;
        }

        public IRestResponse<List<Rss>> RequestRss(DateTime? lastModified = null)
        {
            var client = new RestClient(rssRestUrl);
            var request = new RestRequest();
            request.AddParameter("Timeout", timeout);
            // set "If-Modified-Since" header
            if (lastModified != null)
            {
                request.AddHeader("If-Modified-Since", lastModified.ToString());
            }
            return client.Execute<List<Rss>>(request);
        }

        public List<Action> GetAllActions()
        {
            // find last modified date 
            var dbHandler = DBHandlerGetter.GetHandler();
            var date = dbHandler.GetActionsLastDate();
    
            // execute request
            var response = RequestAction(date);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // get "Last-Modified" header and write to DB
                if (response.Headers.Any(x => x.Name == "Last-Modified"))
                {
                    var lastModify = DateTime.Parse(response.Headers.First(x => x.Name == "Last-Modified").Value.ToString());
                    dbHandler.WriteActionsLastDate(lastModify);
                }
                return response.Data;
            }
            else
                return null;
        }

        public IRestResponse<List<Action>> RequestAction(DateTime? lastModified = null)
        {
            var client = new RestClient(actionsRestUrl);
            var request = new RestRequest();
            request.AddParameter("Timeout", timeout);
            // set "If-Modified-Since" header
            if (lastModified != null)
            {
                request.AddHeader("If-Modified-Since", lastModified.ToString());
            }
            return client.Execute<List<Action>>(request);
        }
    }
}