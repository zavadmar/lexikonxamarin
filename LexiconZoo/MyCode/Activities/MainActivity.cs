﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;
using LexiconZoo.Classes;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace LexiconZoo.Activities
{
    /// <summary>
    /// Main activity with main menu.
    /// </summary>
    [Activity(Label = "Lexikon zvířat", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            // add Microsoft App center analytics and crash reports
            AppCenter.Start("1b44e824-9116-4ff1-9615-74f5cf1c53d2",
                   typeof(Analytics), typeof(Crashes));

            SetContentView(Resource.Layout.MainMenu);

            // set buttons events
            var lexiconBtn = FindViewById<Button>(Resource.Id.lexikonButton);
            lexiconBtn.Click += lexiconBtnClick;
            var mapBtn = FindViewById<Button>(Resource.Id.mapBtn);
            mapBtn.Click += mapBtnClick;

            var rssBtn = FindViewById<Button>(Resource.Id.rssBtn);
            rssBtn.Click += rssBtnClick;
            var actionsBtn = FindViewById<Button>(Resource.Id.calendarBtn);
            actionsBtn.Click += actionsBtnClick;

#if DEBUG
            // Unit tests
            var layout = FindViewById<LinearLayout>(Resource.Id.mainMenuLayout);
            var testButton = new Button(this);
            testButton.Text = "Tests";
            layout.AddView(testButton);
            testButton.Click += (object sender, EventArgs e) =>
            {
                StartActivity(typeof(TestActivity));
            };
#endif
        }

        /// <summary>
        /// Download data. If there are no data, show dialog and download after accept.
        /// If data are downloaded, try update data on background.
        /// </summary>
        protected override void OnResume()
        {
            base.OnResume();

            var dbhandler = DBHandlerGetter.GetHandler();
            if (isDataNotDownloaded(dbhandler))
            {
                downloadData(dbhandler);
            }
            else
            {
                new BackgroundUpdateDownloader(this).Execute();
            }
        }

        /// <summary>
        /// Checks if database has downloaded data.
        /// </summary>
        /// <param name="dbhandler"></param>
        /// <returns></returns>
        private bool isDataNotDownloaded(IDBHandler dbhandler)
        {
            return (dbhandler.GetAnimalCnt() <= 0 || dbhandler.GetActionsCnt() <= 0 || dbhandler.GetRssCnt() <= 0);
        }

        /// <summary>
        /// Show dialog and download data.
        /// </summary>
        /// <param name="dbhandler"></param>
        private void downloadData(IDBHandler dbhandler)
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetMessage(Resource.String.NoDataMessage);

            alert.SetPositiveButton(Resource.String.Download, downloadDialogBtn);
            alert.SetNegativeButton(Resource.String.Close, closeDialogBtn);
            Dialog dialog = alert.Create();
            dialog.Show();
        }

        /// <summary>
        /// Download after positive dialog btn clicked.
        /// </summary>
        private void downloadDialogBtn(object senderAlert, DialogClickEventArgs args)
        {
            (senderAlert as AlertDialog).Dismiss();
            // try download data      
            var success = downloadWithDownloader();
            if (!success)
            {
                // error with data dowload 
                errorDataDownload();
            }
        }

        /// <summary>
        /// Error with data download, show dialog to try again
        /// </summary>
        private void errorDataDownload()
        {            
            AlertDialog.Builder error = new AlertDialog.Builder(this);
            error.SetMessage(Resource.String.ErrorNoDataDownloaded);
            
            error.SetPositiveButton(Resource.String.Download, downloadDialogBtn);
            error.SetNegativeButton(Resource.String.Close, closeDialogBtn);
        }

        /// <summary>
        /// Try download data. If not success return false, else return true.
        /// </summary>
        /// <returns>True if success, else false.</returns>
        private bool downloadWithDownloader()
        {
            var downloader = new CompleteDownloader(this);
            downloader.Execute();
            if (!downloader.actionSuccess || !downloader.animalSuccess || !downloader.rssSuccess)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Close app.
        /// </summary>
        private void closeDialogBtn(object senderAlert, DialogClickEventArgs args)
        {
            (senderAlert as AlertDialog).Dismiss();
#if !DEBUG
            this.FinishAffinity();
#endif
        }

        /// <summary>
        /// Action button click event handler. Open ActionsCalendarActivity.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void actionsBtnClick(object sender, EventArgs e)
        {
            Intent i = new Intent();
            i.SetClass(this, typeof(ActionsCalendarActivity));                                    
            StartActivity(i);            
        }

        /// <summary>
        ///  Rss button click event handler. Open RssListActivity.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rssBtnClick(object sender, EventArgs e)
        {
            Intent i = new Intent();
            i.SetClass(this, typeof(RssListActivity));           
            StartActivity(i);                   
        }

        /// <summary>
        ///  Map button click event handler. Open MapActivity.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mapBtnClick(object sender, EventArgs e)
        {
            StartActivity(typeof(MapActivity));
        }

        /// <summary>
        ///  Lexicon button click event handler. Open LexiconMenuActivity.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void lexiconBtnClick(object sender, EventArgs ea)
        {
            Intent i = new Intent();
            i.SetClass(this,typeof(LexiconMenuActivity));            
            StartActivity(i);            
        }
    }
}

