using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Views;

namespace LexiconZoo.Activities
{
    /// <summary>
    /// Activity for filter menu of lexicon
    /// </summary>
    [Activity(Label = "Lexikon")]
    public class LexiconMenuActivity : Activity
    {
        /// <summary>
        /// Sets events of buttons
        /// </summary>
        /// <param name="savedInstanceState"></param>
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            SetContentView(Resource.Layout.LexiconMenu);
            var alphabetBtn = FindViewById<Button>(Resource.Id.alphabetLexiconBtn);
            alphabetBtn.Click += alphabetBtnClick;

            var taxonomyBtn = FindViewById<Button>(Resource.Id.taxonomyLexiconBtn);
            taxonomyBtn.Click += taxonomyBtnClick;

            var localityBtn = FindViewById<Button>(Resource.Id.localityLexiconBtn);
            localityBtn.Click += localityBtnClick;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    // app icon in action bar clicked; goto parent activity.
                    this.Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        /// <summary>
        /// Method for locality button click event. Open new  LocalityListActivity.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void localityBtnClick(object sender, EventArgs e)
        {
            Intent i = new Intent();
            i.SetClass(this, typeof(LocalityListActivity));
            StartActivity(i);
        }

        /// <summary>
        /// Method for taxonomy button click event. Open new  TaxonomyListActivity.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void taxonomyBtnClick(object sender, EventArgs e)
        {
            Intent i = new Intent();
            i.SetClass(this, typeof(TaxonomyListActivity));
            StartActivity(i);
        }

        /// <summary>
        /// Method for aplhabet button click event. 
        /// Open new AnimalListActivity and set filter to show all animals.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void alphabetBtnClick(object sender, EventArgs e)
        {           
            Intent i = new Intent();
            i.SetClass(this, typeof(AnimalListActivity));
            i.PutExtra("filter", (int)Constants.AdapterFilter.ALL_ANIMALS);
            i.PutExtra("filterValue", "");
            StartActivity(i);
        }
    }
}