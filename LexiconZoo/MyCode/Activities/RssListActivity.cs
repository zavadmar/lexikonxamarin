using Android.App;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;

namespace LexiconZoo.Activities
{
    [Activity(Label = "Novinky")]
    public class RssListActivity : FragmentActivity
    {       
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);         
            SetContentView(Resource.Layout.RssListActivity);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    // app icon in action bar clicked; goto parent activity.
                    this.Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}