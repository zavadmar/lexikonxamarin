using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using LexiconZoo.Classes;
using static Android.Gms.Maps.GoogleMap;
using Android.Views;

namespace LexiconZoo.Activities
{
    /// <summary>
    /// ASctivvity to show Google map with Zoo localities.
    /// </summary>
    [Activity(Label = "Mapa Zoo")]
    public class MapActivity : Activity, IOnMapReadyCallback, IOnPolygonClickListener, IOnMapClickListener, IOnInfoWindowClickListener
    {
        // default zoom in value
        private const int zoomIn = 17;
        // default zoom out value
        private const int zoomOut = 15;
        public static string MARKER_TITLE = "marker";
        // Google map
        private GoogleMap mMap;
        // dictrionary polygon.id -> marker object
        private Dictionary<string, Marker> polygonToMarker;
        // marker to open if activity has been opened from animal detail
        private string openMarker;
        // zoom to marker if activity has been opened from animal detail
        private string zoomToMarkerKey = "";
        // center coordinates of Prague Zoo
        private LatLng zooCenter;        

        /// <summary>
        /// Sets map.
        /// </summary>
        /// <param name="googleMap">Created Google map.</param>
        public void OnMapReady(GoogleMap googleMap)
        {
            mMap = googleMap;
            // enable showing position of device on map
            mMap.MyLocationEnabled = true;
            // enable zoom control
            mMap.UiSettings.ZoomControlsEnabled = true;
            // enable compass
            mMap.UiSettings.CompassEnabled = true;            
            // enable MyLocation button
            mMap.UiSettings.MyLocationButtonEnabled = true;
            // set center coordinates of Prague Zoo
            zooCenter = new LatLng(50.117826, 14.406047);

            // sets localities on map
            setMarkersAndPolygons();

            LatLng location;
            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            Marker marker;
            // zoom to locality, if activity has been opened from animal detail
            if (polygonToMarker.TryGetValue(zoomToMarkerKey, out marker))
            {                                
                builder.Target(marker.Position);
                marker.Visible = true;
                marker.ShowInfoWindow();
                builder.Zoom(zoomIn);
            } // else zoom to zoo center
            else
            {
                location = zooCenter;
                builder.Target(location);
                builder.Zoom(zoomOut);
            }                            
                    
            CameraPosition cameraPosition = builder.Build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            mMap.MoveCamera(cameraUpdate);           

            // setting click listeners
            mMap.SetOnPolygonClickListener(this);
            mMap.SetOnMapClickListener(this);
            mMap.SetOnInfoWindowClickListener(this);
        }

        /// <summary>
        /// sets zoo localoties to map
        /// </summary>
        private void setMarkersAndPolygons()
        {
            zoomToMarkerKey = "";
            // create locations
            var locations = MapLocationsCreator.CreateLocations();
            polygonToMarker = new Dictionary<string, Marker>();
            foreach (var location in locations)
            {
                // add polygon to map
                var polygon = mMap.AddPolygon(location.GetPolygon());
                // add marker to map
                var marker = mMap.AddMarker(location.GetMarker());
                
                // add polygon id and marker object to dictionary
                polygonToMarker.Add(polygon.Id, marker);
                // set marker to open if activity has been opened from animal detail
                if (marker.Title == openMarker)
                    zoomToMarkerKey = polygon.Id;
            }            
        }

        /// <summary>
        /// On polygon click event handler. 
        /// Show marker and its info window that is connected with polygon and zoom to it.
        /// </summary>
        /// <param name="polygon"></param>
        public void OnPolygonClick(Polygon polygon)
        {
            Marker marker;
            var get = polygonToMarker.TryGetValue(polygon.Id, out marker);
            foreach (var m in polygonToMarker.Values)
            {
                m.Visible = false;
            }
            if (get)
            {
                marker.Visible = true;
                mMap.AnimateCamera(CameraUpdateFactory.NewLatLngZoom(marker.Position, zoomIn));
                marker.ShowInfoWindow();
            }
        }

        /// <summary>
        /// On map click event handler.
        /// Hide all markers.
        /// </summary>
        /// <param name="point"></param>
        public void OnMapClick(LatLng point)
        {
            foreach (var marker in polygonToMarker.Values)
            {
                marker.Visible = false;
            }            
        }

        /// <summary>
        /// On info window click event handler.
        /// Open AnimalListActivity with localoty filter and filter value equals info window title.
        /// </summary>
        /// <param name="marker"></param>
        public void OnInfoWindowClick(Marker marker)
        {
            var dbhandler = DBHandlerGetter.GetHandler();

            if (DBHandlerGetter.GetHandler().GetAllAnimals().Where(x => x.LocalitiesTitle.Contains(marker.Title)).Count() > 0)
            {                
                Intent i = new Intent();
                i.SetClass(this, typeof(AnimalListActivity));
                i.PutExtra("filter", (int)Constants.AdapterFilter.LOCALITY);
                i.PutExtra("filterValue", marker.Title);
                StartActivity(i);
            }
            else
            {
                Toast.MakeText(this, Resource.String.NoAnimalsInLoc, ToastLength.Short).Show();
            }

        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            openMarker = Intent.GetStringExtra(MARKER_TITLE) ?? "";
            SetContentView(Resource.Layout.MapLayout);
            SetUpMap();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    // app icon in action bar clicked; goto parent activity.
                    this.Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        /// <summary>
        /// Setup map fragment.
        /// </summary>
        private void SetUpMap()
        {
            if (mMap == null)
            {
                FragmentManager.FindFragmentById<MapFragment>(Resource.Id.map).GetMapAsync(this);
            }
        }      
    }
}