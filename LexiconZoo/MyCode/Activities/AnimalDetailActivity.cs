using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LexiconZoo.Fragments;
using LexiconZoo.Classes;
using Android.Support.V4.App;

namespace LexiconZoo.Activities
{
    /// <summary>
    /// Activity for detail of animal
    /// </summary>
    [Activity(Label = "AnimalDetail")]
    public class AnimalDetailActivity : FragmentActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // get id that has been added when activity was created
            var id = Intent.Extras.GetInt("animalId", 3);
            ActionBar.SetDisplayHomeAsUpEnabled(true);             
            // create fragment a give it animal id
            var details = AnimalDetailTabsFragment.NewInstance((int)id);
            var fragmentTransaction = SupportFragmentManager.BeginTransaction();
            fragmentTransaction.Add(Android.Resource.Id.Content, details);
            fragmentTransaction.Commit();
        }

        /// <summary>
        /// Set back button.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    // app icon in action bar clicked; goto parent activity.
                    this.Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}