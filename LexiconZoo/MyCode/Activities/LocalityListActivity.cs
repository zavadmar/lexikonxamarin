using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using LexiconZoo.Adapters;

namespace LexiconZoo.Activities
{
    /// <summary>
    /// Activity for list of localities
    /// </summary>
    [Activity(Label = "Lokace v zoo")]
    public class LocalityListActivity : Activity
    {
        /// <summary>
        /// Sets locality list and adapter and item click event.
        /// </summary>
        /// <param name="savedInstanceState"></param>
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            ActionBar.SetDisplayHomeAsUpEnabled(true);

            SetContentView(Resource.Layout.ListView);

            var localityListView = FindViewById<ListView>(Resource.Id.listView);

            localityListView.ItemClick += locality_ItemClick;

            var adapter = new LocalitiesAdapter(this);

            localityListView.Adapter = adapter;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    // app icon in action bar clicked; goto parent activity.
                    this.Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        /// <summary>
        /// Item click event. Open new AnimalListActivity with locality filter with value of clicked item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void locality_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var listView = sender as ListView;
            var localityString = (string)listView.Adapter.GetItem(e.Position);
            Intent i = new Intent();
            i.SetClass(this, typeof(AnimalListActivity));
            i.PutExtra("filter", (int)Constants.AdapterFilter.LOCALITY);
            i.PutExtra("filterValue", localityString);
            StartActivity(i);
        }
    }
}