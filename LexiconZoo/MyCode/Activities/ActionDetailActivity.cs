using Android.App;
using Android.Content;
using Android.OS;
using LexiconZoo.Fragments;
using Android.Support.V4.App;
using LexiconZoo.Classes;
using Android.Views;

namespace LexiconZoo.Activities
{
    /// <summary>
    /// Activity for detail of action
    /// </summary>
    [Activity(Label = "ActionDetail")]
    public class ActionDetailActivity : FragmentActivity
    {
        public static readonly string ACTION_ID = "action_id";
        private IDBHandler dbHandler;
        /// <summary>
        /// Starts when activity is created. Set Title of page, enable "back" button,
        /// create fragment.
        /// </summary>
        /// <param name="savedInstanceState"></param>
        protected override void OnCreate(Bundle savedInstanceState)
        {            
            dbHandler = new RealmDBHandler();
            base.OnCreate(savedInstanceState);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            var id = Intent.Extras.GetString(ACTION_ID, "");
            this.Title = "Akce " + dbHandler.GetActionById(id).start.ToString("dd.MM.yyyy");
            var details = ActionDetailFragment.newInstance(id);
            var fragmentTransaction = SupportFragmentManager.BeginTransaction();
            fragmentTransaction.Add(Android.Resource.Id.Content, details);
            fragmentTransaction.Commit();
        }
        
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    // back icon in action bar clicked; goto parent activity.
                    this.Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}