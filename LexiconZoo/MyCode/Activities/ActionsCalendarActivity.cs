using Android.App;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;

namespace LexiconZoo.Activities
{
    /// <summary>
    /// Activity for calendar of actions
    /// </summary>
    [Activity(Label = "Akce v zoo")]
    public class ActionsCalendarActivity : FragmentActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            this.SetTitle(Resource.String.actionsInZoo);
            base.OnCreate(savedInstanceState);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            SetContentView(Resource.Layout.CalendarActivity);            
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    // app icon in action bar clicked; goto parent activity.
                    this.Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}