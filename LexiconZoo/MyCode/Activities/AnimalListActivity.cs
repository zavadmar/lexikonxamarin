using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;

namespace LexiconZoo.Activities
{
    /// <summary>
    /// Activity for list of animals
    /// </summary>
    [Activity(Label = "Lexikon")]
    public class AnimalListActivity : FragmentActivity
    {
        protected Constants.AdapterFilter filter = Constants.AdapterFilter.ALL_ANIMALS;
        protected string filterValue = "";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // get filter and filter value that has been added when activity was created            
            this.filter = (Constants.AdapterFilter) Intent.Extras.GetInt("filter");
            this.filterValue = Intent.Extras.GetString("filterValue");

            SetContentView(Resource.Layout.AnimalListActivity);
        }

        /// <summary>
        /// Get value of filter. It is used by list adapter to filter animals by taxonomy class or locality.
        /// </summary>
        /// <returns>Name of taxonomy class or locality to filter.</returns>
        public string GetFilterValue()
        {
            return filterValue;
        }

        /// <summary>
        /// Get filter type. It can be all animals (no filter), taxonomy filter or locality filter. 
        /// It is use by list adapter.
        /// </summary>
        /// <returns>Filter type.</returns>
        public Constants.AdapterFilter GetFilter()
        {
            return filter;
        }
    }
}