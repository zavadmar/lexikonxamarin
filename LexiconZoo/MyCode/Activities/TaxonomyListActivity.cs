using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using LexiconZoo.Adapters;

namespace LexiconZoo.Activities
{
    [Activity(Label = "T��dy")]
    public class TaxonomyListActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            SetContentView(Resource.Layout.ListView);

            var taxonomyListView = FindViewById<ListView>(Resource.Id.listView);

            taxonomyListView.ItemClick += taxonomy_ItemClick;

            var adapter = new TaxonomyAdapter(this);

            taxonomyListView.Adapter = adapter;
        }

        private void taxonomy_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var listView = sender as ListView;
            var classString = (string)listView.Adapter.GetItem(e.Position);
            Intent i = new Intent();
            i.SetClass(this, typeof(AnimalListActivity));
            i.PutExtra("filter", (int)Constants.AdapterFilter.TAXONOMY);
            i.PutExtra("filterValue", classString);
            StartActivity(i);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    // app icon in action bar clicked; goto parent activity.
                    this.Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}