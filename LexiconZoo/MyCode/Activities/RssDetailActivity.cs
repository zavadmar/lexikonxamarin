using Android.App;
using Android.Content;
using Android.OS;
using LexiconZoo.Fragments;
using Android.Support.V4.App;
using LexiconZoo.Classes;
using Android.Views;

namespace LexiconZoo.Activities
{
    /// <summary>
    /// Activity for rss detail
    /// </summary>
    [Activity(Label = "RssDetail")]
    public class RssDetailActivity : FragmentActivity
    {
        public static string RSS_ID = "rss_id";
        private IDBHandler dbHandler;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            dbHandler = new RealmDBHandler();
            base.OnCreate(savedInstanceState);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
            var id = Intent.Extras.GetString(RSS_ID, "");
            this.Title = dbHandler.GetRssById(id).title;
            // create fragment with rss id
            var details = RssDetailFragment.NewInstance(id);
            var fragmentTransaction = SupportFragmentManager.BeginTransaction();
            fragmentTransaction.Add(Android.Resource.Id.Content, details);
            fragmentTransaction.Commit();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    // app icon in action bar clicked; goto parent activity.
                    this.Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}