namespace LexiconZoo
{
    class InsertTestData
    {

        public static void insertData(int cnt = 10)
        {
            var realm = Realms.Realm.GetInstance();
            for (int i = 0; i < cnt; i++)
            {
                var animal = new Animal();
                realm.Write(() =>
                {
                    animal.Title = "Test" + i;
                    animal._id = i;
                    realm.Add(animal);
                });
            }

        }
    }
}