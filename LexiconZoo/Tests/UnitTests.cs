﻿using NUnit.Framework;
using LexiconZoo.Classes;
using System.Linq;

namespace LexiconZoo.Tests
{
    [TestFixture]
    public class UnitTests
    {
        /// <summary>
        /// Test to control if downloaded animals and DB animals are same
        /// </summary>
        [Test]
        public void RestAnimals()
        {
            var restService = new RestService();
            var response = restService.RequestAnimals();
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);
            var animals = response.Data;
            var notDeleted = animals.FindAll(x => x.deleted == false);
            var dbAnimals = DBHandlerGetter.GetHandler().GetAllAnimals().ToList();
            Assert.AreEqual(notDeleted.Count, dbAnimals.Count());            
            foreach (var animal in animals)
            {
                if (animal.deleted)
                {
                    Assert.IsTrue(dbAnimals.Find(x => x._id == animal._id) == null);
                }
                else
                {
                    Assert.IsTrue(dbAnimals.Find(x => x._id == animal._id) != null);
                }
            }
        }

        /// <summary>
        /// Test to control if downloaded actions and DB actions are same
        /// </summary>
        [Test]
        public void RestActions()
        {
            var restService = new RestService();
            var response = restService.RequestAction();
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);
            var actions = response.Data;
            var dbActions = DBHandlerGetter.GetHandler().GetAllActions().ToList();
            var notDeleted = actions.FindAll(x => x.deleted == false);
            Assert.AreEqual(notDeleted.Count, dbActions.Count());
            foreach (var action in actions)
            {
                if (action.deleted)
                {
                    Assert.IsTrue(dbActions.Find(x => x._id == action._id) == null);
                }
                else
                {
                    Assert.IsTrue(dbActions.Find(x => x._id == action._id) != null);
                }
            }
        }

        /// <summary>
        /// Test to control if downloaded rss and DB rss are same
        /// </summary>
        [Test]
        public void RestRss()
        {
            var restService = new RestService();
            var response = restService.RequestRss();
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);
            var rss = response.Data;
            var dbRss = DBHandlerGetter.GetHandler().GetAllRss().ToList();
            var notDeleted = rss.FindAll(x => x.deleted == false);
            Assert.AreEqual(notDeleted.Count, dbRss.Count());
            foreach (var r in rss)
            {
                if (r.deleted)
                {
                    Assert.IsTrue(dbRss.Find(x => x._id == r._id) == null);
                }
                else
                {
                    Assert.IsTrue(dbRss.Find(x => x._id == r._id) != null);
                }
            }
        }

        /// <summary>
        /// Test if all map points are in Zoo
        /// </summary>
        [Test]
        public void MapLocationInZoo()
        {
            var locations = MapLocationsCreator.CreateLocations();
            foreach (var loc in locations)
            {
                foreach (var p in loc.getMyPoints())
                {
                    Assert.IsTrue((p.latitude > 50.113607 ) && (p.latitude < 50.121870), "Latitude out of range: " + p.latitude + " in " + loc.Title);
                    Assert.IsTrue((p.longitude > 14.395443) && (p.longitude < 14.413969), "Longitude out of range: " + p.longitude + " in " + loc.Title);
                }

                Assert.IsTrue((loc.getMyMarkerPoint().latitude > 50.113607) && (loc.getMyMarkerPoint().latitude < 50.121870), "Marker latitude out of range: " + loc.getMyMarkerPoint().latitude + " in " + loc.Title);
                Assert.IsTrue((loc.getMyMarkerPoint().longitude > 14.395443) && (loc.getMyMarkerPoint().longitude < 14.413969), "Marker longitude out of range: " + loc.getMyMarkerPoint().longitude + " in " + loc.Title);
            }
        }
    }
}
