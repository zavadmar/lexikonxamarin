﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
using Xamarin.UITest.Android;

namespace UITest
{
    [TestFixture]
    public class Tests
    {
        AndroidApp app;

        [SetUp]
        public void BeforeEachTest()
        {
            // TODO: If the Android app being tested is included in the solution then open
            // the Unit Tests window, right click Test Apps, select Add App Project
            // and select the app projects that should be tested.
            app = ConfigureApp
                .Android
             //   .Debug()
                // TODO: Update this path to point to your Android app and uncomment the
                // code if the app is not included in the solution.
              //  .ApkFile ("../../../LexiconZoo/bin/Release/com.zavadil.lexiconzoo.apk")
                .StartApp();
            DownloadData();
        }

      //  [Test]
        public void AppLaunches()
        {
            app.Repl();
            //app.Screenshot("First screen.");
        }

     
        public void DownloadData()
        {
            app.WaitForElement("Stáhnout");
            app.Tap("Stáhnout");
            app.WaitForNoElement("Stáhnout", "Timeout pri stahovani dat", new TimeSpan(0,2,0));
            app.WaitForElement("Stahuji data...");
            app.WaitForNoElement("Stahuji data...");
            //app.Screenshot("First screen.");
        }

        [Test]
        public void AnimalBasicTest()
        {
            app.WaitForElement("lexikonButton");
            app.Tap("lexikonButton");
            app.WaitForElement("alphabetLexiconBtn");
            app.Tap("alphabetLexiconBtn");
            app.WaitForElement("AnimalListView");
            var title1 = app.Query("title").First();
            Assert.AreEqual(title1.Text, "Achatina žravá", "Není načteno očekávané. Zkontrolovat stahování a případnou změnu zdrojových dat.");
            // Detail
            app.Tap("title");
            app.WaitForElement("LatinTitle");
            Assert.AreEqual(title1.Text, app.Query("Title").First().Text);
            app.SwipeRightToLeft();
            app.SwipeRightToLeft();
            // map btn
            app.Tap("Ukázat na mapě");
            app.WaitForElement("Mapa Zoo");
            app.Back();
            app.WaitForElement("attraction");
            app.Back();
            // change card -> list
            app.WaitForElement("AnimalListView");
            var cardCount = app.Query("title").Count();
            app.Tap("changeView");
            var listCount = app.Query("title").Count();
            Assert.GreaterOrEqual(listCount, cardCount);
        }

        [Test]
        public void AnimalSearchTest()
        {
            app.WaitForElement("lexikonButton");
            app.Tap("lexikonButton");
            app.WaitForElement("alphabetLexiconBtn");
            app.Tap("alphabetLexiconBtn");
            app.WaitForElement("AnimalListView");

            // search
            app.Tap("search_button");
            app.EnterText("search_edit_frame", "zi");
            app.EnterText("search_edit_frame", "raf");
            app.EnterText("a");
            app.WaitForNoElement("Achatina žravá");
            
            Assert.IsTrue(app.Query("title").First().Text.Contains("Žirafa"));
        }

        [Test]
        public void AnimalTaxonomyListTest()
        {
            app.WaitForElement("lexikonButton");
            app.Tap("lexikonButton");
            app.WaitForElement("alphabetLexiconBtn");

            app.Tap("taxonomyLexiconBtn");
            app.WaitForElement("listView");
            var tax = app.Query("text1").First().Text;
            app.Tap("text1");
            app.WaitForElement("AnimalListView");
            app.Tap("title");
            app.WaitForElement("LatinTitle");
            Assert.AreEqual("Třída: " + tax, app.Query("animalClass").First().Text);
        }

        [Test]
        public void AnimalLocalityListTest()
        {
            app.WaitForElement("lexikonButton");
            app.Tap("lexikonButton");
            app.WaitForElement("alphabetLexiconBtn");
            app.Tap("localityLexiconBtn");
            app.WaitForElement("listView");
            var loc = app.Query("text1").First().Text;
            app.Tap("text1");
            app.WaitForElement("AnimalListView");
            app.Tap("title");
            app.WaitForElement("LatinTitle");
            app.SwipeRightToLeft();
            app.SwipeRightToLeft();
            Assert.IsTrue(app.Query("attraction").First().Text.Contains(loc));
        }

        [Test]
        public void RssTest()
        {         
            app.WaitForElement("rssBtn");
            app.Tap("rssBtn");
            app.WaitForElement("list");
            var titleList = app.Query(c => c.Marked("text1")).First().Text;
            var dateList = app.Query(c => c.Marked("text2")).First().Text;
            app.Tap("text1");
            app.WaitForElement("title");
            var title = app.Query(c => c.Marked("title")).First().Text;
            var date = app.Query(c => c.Marked("date")).First().Text;
            Assert.AreEqual(titleList, title);
            Assert.AreEqual(dateList, date);
        }

        [Test]
        public void ActionsTest()
        {         
            app.WaitForElement("calendarBtn");
            app.Tap("calendarBtn");
            app.WaitForElement("calendar_weekdays");
            AppResult text1 = findClickable();
            
            if (text1 == null)
            {
                app.SwipeRightToLeft();
                text1 = findClickable();
                if (text1 == null)
                {
                    Assert.IsTrue(false, "Nenalezena žádná akce v kalendáři na tento nebo příští měsíc.");                     
                }
            }
            var title = text1.Text;
            app.Tap(text1.Text);
            app.WaitForElement("title");
            Assert.AreEqual(title, app.Query(title).First().Text);
        }

        private AppResult findClickable(int addMonths = 0)
        {
            int start = addMonths == 0 ? DateTime.Now.Day : 1;
            var date = DateTime.Now.AddMonths(addMonths);
            int stop = DateTime.DaysInMonth(date.Year, date.Month);
            for (int i = start; i <= stop; i++)
            {
                app.Tap(i.ToString());
                var texts = app.Query("text1");
                if (texts.Count() < 1) continue;
                return texts.First();
            }
            return null;
        }
    }
}

